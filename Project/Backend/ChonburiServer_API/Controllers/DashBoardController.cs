﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ChonburiServer_API.Services.Interfaces;
using ChonburiServer_API.Models;
using System;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.Models.SignAuthen;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using ChonburiServer_API.Services.Configure;
using ChonburiServer_API.Services.Interfaces.Staff;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using ChonburiServer_API.Models.Filter;
using System.Threading.Tasks;
using ChonburiServer_API.Models.Participant;
using ChonburiServer_API.Services.Interfaces.Participant;
using ChonburiServer_API.Models.HomePage;
using ChonburiServer_API.Services.Interfaces.HomePage;
using ChonburiServer_API.Services.Interfaces.DashBoard;
using ChonburiServer_API.Models.DashBoard;
using Microsoft.AspNetCore.Authorization;

namespace ChonburiServer_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]

    public class DashBoardController : ControllerBase
    {
        private readonly IDashBoardService _service;
       
        public DashBoardController(IDashBoardService service)
        {
            _service = service;
        }
        #region DashBoard
        [HttpGet]
        public DashBoardResultModel GeCoutDatatDashBoard()
        {
            #region code
            var result = new DashBoardResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GeCoutDatatDashBoard();
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GeCoutDatatHomePage " + ex.Message;
            }
            #endregion
            return result;
        }
        
        #endregion

    }
}
