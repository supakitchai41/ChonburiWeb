﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ChonburiServer_API.Services.Interfaces;
using ChonburiServer_API.Models;
using System;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.Models.SignAuthen;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using ChonburiServer_API.Services.Configure;
using ChonburiServer_API.Services.Interfaces.Staff;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using ChonburiServer_API.Models.Filter;
using System.Threading.Tasks;
using ChonburiServer_API.Models.Participant;
using ChonburiServer_API.Services.Interfaces.Participant;
using ChonburiServer_API.Models.HomePage;
using ChonburiServer_API.Services.Interfaces.HomePage;
using Microsoft.AspNetCore.Authorization;

namespace ChonburiServer_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomePageController : Controller
    {
        private readonly IHomePageService _service;
       
        public HomePageController(IHomePageService service)
        {
            _service = service;
        }
        #region Staff
        [HttpGet]
        public HomePageResultModel GeCoutDatatHomePage()
        {
            #region code
            var result = new HomePageResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GeCoutDatatHomePage();
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GeCoutDatatHomePage " + ex.Message;
            }
            #endregion
            return result;
        }

        

        #endregion

    }
}
