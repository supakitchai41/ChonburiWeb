﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ChonburiServer_API.Services.Interfaces;
using ChonburiServer_API.Models;
using System;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.Models.SignAuthen;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using ChonburiServer_API.Services.Configure;
using ChonburiServer_API.Services.Interfaces.Staff;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using ChonburiServer_API.Models.Filter;
using System.Threading.Tasks;
using ChonburiServer_API.Models.Participant;
using ChonburiServer_API.Services.Interfaces.Participant;
using Microsoft.AspNetCore.Authorization;

namespace ChonburiServer_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class ParticipantController : Controller
    {
        private readonly IParticipantService _service;
       
        public ParticipantController(IParticipantService service)
        {
            _service = service;
        }
        #region Participant
        [HttpGet]
        [Route("GetParticipants")]
        public ParticipantResultModels GetParticipants([FromQuery] ParticipantFilterModel model)
        {
            var result = new ParticipantResultModels
            {
                status = 200,
                success = true,

            };
            try
            {
               var data = _service.Gets(model);
               result = data; 
            }
            catch(Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "Gets" + ex.Message;

            }
            #region code

            return result;
            #endregion
        }
        [HttpGet]
        [Route("GetParticipantById/{id:int}")]
        public ParticipantResultModel GetParticipantById([FromRoute] int id)
        {
            #region code
            var result = new ParticipantResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GetById(id);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GetStaffById" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpPost]
        [Route("Create")]
        public ResultModel Create(ParticipantModel model)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.Crate(model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "Create" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpPut]
        [Route("Update/{id:int}")]
        public ResultModel Update( int id, ParticipantModel model)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.Update(id, model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "Update" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpDelete]
        [Route("Delete/{id:int}")]
        public ResultModel Delete(int id)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.Delete(id);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "Delete" + ex.Message;
            }
            #endregion
            return result;
        }

        #endregion

        #region All Type in Participant
        [HttpGet]
        [Route("GetAllTypeInParticipant")]
        public AllTypeInParticipantModel GetAllTypeInParticipant()
        {
            var result = new AllTypeInParticipantModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GetAllTypeInParticipant();
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "Gets" + ex.Message;

            }
            #region code

            return result;
            #endregion
        }
        #endregion

    }
}
