﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ChonburiServer_API.Services.Interfaces;
using ChonburiServer_API.Models;
using System;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.Models.SignAuthen;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using ChonburiServer_API.Services.Configure;
using ChonburiServer_API.Services.Interfaces.Staff;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using ChonburiServer_API.Models.Filter;
using System.Threading.Tasks;
using ChonburiServer_API.Models.DashBoard;
using Microsoft.AspNetCore.Authorization;

namespace ChonburiServer_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StaffController : Controller
    {
        private readonly IStaffService _service;
        public StaffController(IStaffService service)
        {
            _service = service;
        }
        #region Staff
        [HttpGet]
        [Route("GetStaffs")]
        public StaffResultModels GetStaffs([FromQuery] StaffFilterModel model)
        {
            var result = new StaffResultModels
            {
                status = 200,
                success = true,

            };
            try
            {
               var data = _service.GetStaffs(model);
               result = data; 
            }
            catch(Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GetStaffs" + ex.Message;

            }
            #region code

            return result;
            #endregion
        }
        [HttpGet]
        [Route("GetStaffById/{id:int}")]
        public StaffResultModel GetStaffById([FromRoute] int id)
        {
            #region code
            var result = new StaffResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GetStaffById(id);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GetStaffById" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpPost]
        [Route("CreateStaff")]
        public ResultModel CreateStaff(StaffsModel model)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.CrateStaffById(model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "CreateStaff" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpPut]
        [Route("UpdateStaff/{id:int}")]
        public ResultModel UpdateStaff( int id,StaffsModel model)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.UpdateStaffById(id, model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "UpdateStaff" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpDelete]
        [Route("DeleteStaff/{id:int}")]
        public ResultModel DeleteStaff(int id)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.DeleteStaffById(id);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "DeleteStaff" + ex.Message;
            }
            #endregion
            return result;
        }

        #endregion


        #region role

        [HttpGet]
        [Route("GetRoles")]
        public async Task<ActionResult<RoleResultModels>>  GetRoles([FromQuery] RoleFilterModel model)
        {
            #region code
            var result = new RoleResultModels
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = await _service.GetRoles(model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GetRoles" + ex.Message;
            }
            #endregion
            return result;
        }
        [HttpGet]
        [Route("GetRoleById/{id:int}")]
        public RoleResultModel GetRoleById([FromRoute]int id)
        {
            #region code
            var result = new RoleResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GetRoleById(id);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GetRoleById" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpPost]
        [Route("CrateRole")]
        public ResultModel CrateRole(RoleModel model)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.CrateRoleById(model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "CrateRole" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpPut]
        [Route("UpdateRol/{id:int}")]
        public ResultModel UpdateRole(int id, RoleModel model)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.UpdateRoleById(id, model);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "UpdateRole" + ex.Message;
            }
            #endregion
            return result;
        }

        [HttpDelete]
        [Route("DeleteRole/{id:int}")]
        public ResultModel DeleteRole(int id)
        {
            #region code
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.DeleteRoleById(id);
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "DeleteRole" + ex.Message;
            }
            #endregion
            return result;
        }

        #endregion
    }
}
