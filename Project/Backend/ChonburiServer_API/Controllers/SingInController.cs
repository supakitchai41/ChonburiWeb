﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using ChonburiServer_API.Services.Interfaces;
using System;
using ChonburiServer_API.Models.SignAuthen;
using ChonburiServer_API.Models.Staff;

namespace ChonburiServer_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SingInController : Controller
    {
        private readonly ISignAuthenService _service;
        public SingInController(ISignAuthenService service)
        {
            _service = service;
        }

        [HttpPost]
        public StaffResultModel SingIn(SingInModel authen)
        {
           var result = new StaffResultModel
           {
               status = 200,
               success = true,
           };

            try
            {
                var staff = _service.EmpAuthen(authen);
                if (staff.success)
                {
                    var token = _service.GenerateToken(staff.data);
                    if (token != null)
                        {
                        Response.Headers.Add("token", token);
                        result.data = staff.data;
                        result.message = token;
                    }
                    else
                    {
                        result.message = "SingIn failed : Not Token ";
                        result.success = false;
                        result.status = 500;
                    }
                }
                else
                {
                    result.message = "SingIn failed : Not  Staff ";
                    result.success = false;
                    result.status = 500;
                }

            }
            catch (Exception ex)
            {
                result.message = "SingIn failed : " + ex.Message;
                result.success = false;
                result.status = 500;
            }
            return result;
        }

    }
}
