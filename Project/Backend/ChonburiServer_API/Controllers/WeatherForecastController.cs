﻿using ChonburiServer_API.Models.DashBoard;
using ChonburiServer_API.Services.Interfaces.DashBoard;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChonburiServer_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IDashBoardService _service;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, IDashBoardService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
        #region DashBoard
        [HttpGet]
        [Route("GeCoutDatatDashBoard")]

        //[Authorize]
        public DashBoardResultModel GeCoutDatatDashBoard()
        {
            #region code
            var result = new DashBoardResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = _service.GeCoutDatatDashBoard();
                result = data;
            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GeCoutDatatHomePage " + ex.Message;
            }
            #endregion
            return result;
        }

        #endregion
    }
}
