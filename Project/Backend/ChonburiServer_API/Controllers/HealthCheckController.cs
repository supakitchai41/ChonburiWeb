﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ChonburiServer_API.Services.Interfaces;
using ChonburiServer_API.Models;
using System;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.Models.SignAuthen;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using ChonburiServer_API.Services.Configure;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.Staff.Role;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Reflection;

namespace ChonburiServer_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthCheckController : Controller
    {

        [HttpGet]
        public string GetEnvironment()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileInfo file = new FileInfo(assembly.Location);
            string dateModified = file.LastWriteTime.ToString("dd/MM/yyyy : HH:mm:ss");
            var hostName = Dns.GetHostName();
            return $"env = {env}, modified = {dateModified}, host = {hostName}";
        }


    }
}
