﻿
using ChonburiServer_API.DataAccess;
using Microsoft.AspNetCore.Mvc;
using ChonburiServer_API.Services;
using System.Collections.Generic;
using ChonburiServer_API.Models;
using System;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.Models.Participant;
using ChonburiServer_API.Services.Interfaces.Participant;
using Microsoft.AspNetCore.Authorization;

namespace ChonburiServer_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class ExcelController : Controller
    {
        private readonly IParticipantService _service;
        public ExcelController(IParticipantService service)
        {
            _service = service;
        }

        //[HttpGet]
        //public ResultModel ReadExcel()
        //{
        //   var result = new ResultModel
        //   {
        //       status = 200,
        //       success = true,
        //   };

        //    try
        //    {
        //        string excelFilePath = "C:\\Chonburi\\ExcelFile.xlsx";
        //        var excelModels = new List<ParticipantModel>();
        //        using (XLWorkbook workbook = new XLWorkbook(excelFilePath))
        //        {
        //            IXLWorksheet worksheet = workbook.Worksheet(1); // Select the first worksheet

        //            IXLRange range = worksheet.RangeUsed();

        //            foreach (IXLRangeRow row in range.RowsUsed())
        //            {
        //                var excelModel = new ParticipantModel()
        //                {
        //                    ParticipantSkillModels = new List<ParticipantSkillModel>()
        //                };
        //                var i =1;
        //                foreach (IXLCell cell in row.Cells())
        //                {
        //                    string cellValue = cell.Value.ToString();
        //                    if (i == 1){i++;continue;}
        //                    if (i == 2)
        //                    {
        //                        excelModel.Sex = Convert.ToInt32(cellValue) ;
        //                    }
        //                    else if (i == 3)
        //                    {
        //                        excelModel.TitleName = cellValue;
        //                    }
        //                    else if (i == 4)
        //                    {
        //                        excelModel.CustName = cellValue;
        //                    }
        //                    else if (i == 5)
        //                    {
        //                        excelModel.CustSurename = cellValue;
        //                    }
        //                    else if (i == 6)
        //                    {
        //                        excelModel.Birthday = cellValue == "-" ?null :cellValue ;
        //                    }
        //                    else if (i == 7)
        //                    {
        //                        excelModel.Age = cellValue == "-" ? 0 : Convert.ToInt32(cellValue);
        //                    }
        //                    else if (i == 8)
        //                    {
        //                        excelModel.DistrictOfResidence = cellValue == "-" ? 0 :  Convert.ToInt32(cellValue) ;
        //                    }
        //                    else if (i == 9)
        //                    {
        //                        excelModel.Phone = cellValue == "-" ? "" : cellValue;
        //                    }
        //                    else if (i == 10)
        //                    {
        //                        excelModel.ParticipantType = cellValue == "-" ? 0 :  Convert.ToInt32(cellValue);
        //                    }
        //                    else if (i == 11 && (cellValue != null && cellValue != ""))
        //                    {
        //                        var ParticipantSkillModel = new ParticipantSkillModel
        //                        {
        //                            IdSkill = Convert.ToInt32(cellValue),
        //                        };
        //                        excelModel.ParticipantSkillModels.Add(ParticipantSkillModel);
        //                    }
        //                    else if (i == 12 && (cellValue != null && cellValue != ""))
        //                    {
        //                        var ParticipantSkillModel = new ParticipantSkillModel
        //                        {
        //                            IdSkill = Convert.ToInt32(cellValue),
        //                        };
        //                        excelModel.ParticipantSkillModels.Add(ParticipantSkillModel);

        //                    }
        //                    else if (i == 13 && (cellValue != null && cellValue != ""))
        //                    {
        //                        var ParticipantSkillModel = new ParticipantSkillModel
        //                        {
        //                            IdSkill = Convert.ToInt32(cellValue),
        //                        };
        //                        excelModel.ParticipantSkillModels.Add(ParticipantSkillModel);
        //                    }
        //                    i++;
        //                }
        //                excelModels.Add(excelModel);
        //            }
        //        }
        //        var data = _service.Crates(excelModels);
        //        result = data;

        //    }
        //    catch (Exception ex)
        //    {
        //        result.message = "ReadExcel : " + ex.Message;
        //        result.success = false;
        //        result.status = 500;
        //    }
        //    return result;
        //}

    }
}
