using ChonburiServer_API.DataAccess.Chonburi.Context;
using ChonburiServer_API.Services;
using ChonburiServer_API.Services.DashBoard;
using ChonburiServer_API.Services.HomePage;
using ChonburiServer_API.Services.Interfaces;
using ChonburiServer_API.Services.Interfaces.DashBoard;
using ChonburiServer_API.Services.Interfaces.HomePage;
using ChonburiServer_API.Services.Interfaces.Participant;
using ChonburiServer_API.Services.Interfaces.Staff;
using ChonburiServer_API.Services.Participant;
using ChonburiServer_API.Services.Project;
using ChonburiServer_API.Services.Staff;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChonburiServer_API
{
    public class Startup
    {
        private readonly string _dbStringConnection = "";
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
              .AddJsonFile("message.json", optional: true, reloadOnChange: true)
              .AddEnvironmentVariables();
            Configuration = builder.Build();

            this._dbStringConnection = Configuration.GetConnectionString("DbChonburiContext");

        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddHttpClient();

            #region "swagger"
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "ChonburiServer API",
                    Version = "v1",
                    Description = "Description for the API goes here.",
                    Contact = new OpenApiContact
                    {
                        Name = "Ankush Jain",
                        Email = string.Empty,
                        Url = new Uri("https://coderjony.com/"),
                    },
                });
                c.AddSecurityDefinition("Bearer",
                    new OpenApiSecurityScheme
                    {
                        Scheme = "bearer",
                        BearerFormat = "JWT",
                        Name = "Authentication",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.Http,
                        Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",
                    });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                      new OpenApiSecurityScheme
                      {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        },
                        //Scheme = "oauth2",
                        //Name = "Bearer",
                        //In = ParameterLocation.Header,
                      },
                            new string[]{}
                    }
                });
            });

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo
            //    {
            //        Version = "v1",
            //        Title = "Chonburi API",
            //        Description = "A simple example ASP.NET Core Web API",
            //        TermsOfService = new Uri("https://example.com/terms")
            //    });
            //    c.AddSecurityDefinition("Bearer",
            //        new OpenApiSecurityScheme
            //        {
            //            Scheme = "bearer",
            //            BearerFormat = "JWT",
            //            Name = "JWT Authentication",
            //            In = ParameterLocation.Header,
            //            Type = SecuritySchemeType.Http,
            //            Description = "Put **_ONLY_** your JWT Bearer token on textbox below!",
            //        });
            //    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
            //    {
            //        {
            //          new OpenApiSecurityScheme
            //          {
            //            Reference = new OpenApiReference
            //              {
            //                Type = ReferenceType.SecurityScheme,
            //                Id = "Bearer"
            //              },
            //              Scheme = "oauth2",
            //              Name = "Bearer",
            //              In = ParameterLocation.Header,

            //            },
            //            new List<string>()
            //        }
            //    });

            //    var basePath = AppContext.BaseDirectory;
            //    var xmlPath = Path.Combine(basePath, "ChonburiServer_API.xml");
            //    c.IncludeXmlComments(xmlPath);
            //});
            #endregion

            #region "authentication"
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    ValidateLifetime = true,
                    RequireExpirationTime = true,
                    ClockSkew = TimeSpan.Zero,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };
            });
            #endregion

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddMemoryCache();

            #region New DB
            services.AddDbContext<ChonburiContext>(options => options.UseSqlServer(this._dbStringConnection));
            #endregion

            //

            #region New Service
            services.AddScoped<IStaffService, StaffService>();
            services.AddScoped<IParticipantService, ParticipantService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IHomePageService, HomePageService>();
            services.AddScoped<IDashBoardService, DashBoardService>();
            services.AddScoped<ISignAuthenService, SignAuthenService>();
            #endregion
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseHsts();
                app.UseExceptionHandler("/Error");
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("./v1/swagger.json", "API V1");
            });

            app.UseHttpsRedirection();
            app.UseRouting();
            //app.UseEnyimMemcached();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors(policy => policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
