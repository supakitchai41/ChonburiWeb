﻿using ChonburiServer_API.DataAccess.Chonburi.Context;
using ChonburiServer_API.Models.DashBoard;
using ChonburiServer_API.Models.HomePage;
using ChonburiServer_API.Services.Interfaces.DashBoard;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ChonburiServer_API.Services.DashBoard
{
    public class DashBoardService : IDashBoardService
    {
        private readonly ChonburiContext _context;
        public DashBoardService(ChonburiContext context)
        {
            this._context = context;
        }

        public DashBoardResultModel GeCoutDatatDashBoard()
        {
            var result = new DashBoardResultModel
            {
                success = true,
                status = 200
            };

            try
            {
                var getDataList = this.GetAllData();
                #region Setting Data count
                List<CountSexModel> countSexModels = new List<CountSexModel>();
                countSexModels = getDataList.sexModels;

                List<CountSkillModel> countCountSkillModels = new List<CountSkillModel>();
                countCountSkillModels = getDataList.skillModels;

                List<CountAreaResidenceModel> countAreaResidenceModels = new List<CountAreaResidenceModel>();
                countAreaResidenceModels = getDataList.areaResidenceModels;

                List<CountParticipantTypeModel> countParticipantTypeModels = new List<CountParticipantTypeModel>();
                countParticipantTypeModels = getDataList.participantTypeModels;
                #endregion

                #region Result Model
                List<CountSexModel> sexResult = new List<CountSexModel>();
                List<CountParticipantTypeModel> participantTypeResult = new List<CountParticipantTypeModel>();
                List<CountAreaResidenceModel> areaResidenceResult = new List<CountAreaResidenceModel>();
                List<CountSkillModel> skillResult = new List<CountSkillModel>();
                #endregion

                var query = _context.tbParticipant
                               .Where(a => !a.IsDeleted)
                               .AsQueryable();
                var countParticipant = query.Count();
                var sexCuont = query.Select(s => new IdModel
                {
                    Id = s.Sex
                }).ToList();
                var areaResidenceCuont = query.Select(s => new IdModel
                {
                    Id = s.DistrictOfResidence
                }).ToList();
                var participantTypeCuont = query.Select(s => new IdModel
                {
                    Id = s.ParticipantType
                }).ToList();
               
                var qureySkill = _context.tbParticipantSkill.Where(w => query.Select(s => s.Id).Contains(w.IdParticipant)).Select(s => new
                {
                    Id = s.IdSkill,
                }).ToList();
                if (sexCuont.Count > 0 && countSexModels.Count > 0)
                {
                    var sexModel1 = countSexModels.Where(w => w.Id == 1).FirstOrDefault();
                    sexModel1.Count = sexCuont.Where(w => w.Id == 1).Count();
                    var sexModel2 = countSexModels.Where(w => w.Id == 2).FirstOrDefault();
                    sexModel2.Count = sexCuont.Where(w => w.Id == 2).Count();
                    var sexModel3 = countSexModels.Where(w => w.Id == 3).FirstOrDefault();
                    sexModel3.Count = sexCuont.Where(w => w.Id == 3).Count();
                    var sexModel4 = countSexModels.Where(w => w.Id == 4).FirstOrDefault();
                    sexModel4.Count = sexCuont.Where(w => w.Id == 4).Count();
                    sexResult.Add(sexModel1);
                    sexResult.Add(sexModel2);
                    sexResult.Add(sexModel3);
                    sexResult.Add(sexModel4);
                }
                if (areaResidenceCuont.Count > 0 && countAreaResidenceModels.Count > 0)
                {

                    var sexModel1 = countAreaResidenceModels.Where(w => w.Id == 1).FirstOrDefault();
                    sexModel1.Count = areaResidenceCuont.Where(w => w.Id == 1).Count();

                    var sexModel2 = countAreaResidenceModels.Where(w => w.Id == 2).FirstOrDefault();
                    sexModel2.Count = areaResidenceCuont.Where(w => w.Id == 2).Count();

                    var sexModel3 = countAreaResidenceModels.Where(w => w.Id == 3).FirstOrDefault();
                    sexModel3.Count = areaResidenceCuont.Where(w => w.Id == 3).Count();

                    var sexModel4 = countAreaResidenceModels.Where(w => w.Id == 4).FirstOrDefault();
                    sexModel4.Count = areaResidenceCuont.Where(w => w.Id == 4).Count();

                    var sexModel5 = countAreaResidenceModels.Where(w => w.Id == 5).FirstOrDefault();
                    sexModel5.Count = areaResidenceCuont.Where(w => w.Id == 5).Count();

                    areaResidenceResult.Add(sexModel1);
                    areaResidenceResult.Add(sexModel2);
                    areaResidenceResult.Add(sexModel3);
                    areaResidenceResult.Add(sexModel4);
                    areaResidenceResult.Add(sexModel5);
                }
                if (participantTypeCuont.Count > 0 && countParticipantTypeModels.Count > 0)
                {
                    var sexModel1 = countParticipantTypeModels.Where(w => w.Id == 1).FirstOrDefault();
                    sexModel1.Count = participantTypeCuont.Where(w => w.Id == 1).Count();

                    var sexModel2 = countParticipantTypeModels.Where(w => w.Id == 2).FirstOrDefault();
                    sexModel2.Count = participantTypeCuont.Where(w => w.Id == 2).Count();

                    var sexModel3 = countParticipantTypeModels.Where(w => w.Id == 3).FirstOrDefault();
                    sexModel3.Count = participantTypeCuont.Where(w => w.Id == 3).Count();

                    var sexModel4 = countParticipantTypeModels.Where(w => w.Id == 4).FirstOrDefault();
                    sexModel4.Count = participantTypeCuont.Where(w => w.Id == 4).Count();
                    participantTypeResult.Add(sexModel1);
                    participantTypeResult.Add(sexModel2);
                    participantTypeResult.Add(sexModel3);
                    participantTypeResult.Add(sexModel4);
                }
                if (qureySkill.Count > 0 && countCountSkillModels.Count > 0)
                {
                    var sexModel1 = countCountSkillModels.Where(w => w.Id == 1).FirstOrDefault();
                    sexModel1.Count = qureySkill.Where(w => w.Id == 1).Count();

                    var sexModel2 = countCountSkillModels.Where(w => w.Id == 2).FirstOrDefault();
                    sexModel2.Count = qureySkill.Where(w => w.Id == 2).Count();

                    var sexModel3 = countCountSkillModels.Where(w => w.Id == 3).FirstOrDefault();
                    sexModel3.Count = qureySkill.Where(w => w.Id == 3).Count();

                    var sexModel4 = countCountSkillModels.Where(w => w.Id == 4).FirstOrDefault();
                    sexModel4.Count = qureySkill.Where(w => w.Id == 4).Count();

                    var sexModel5 = countCountSkillModels.Where(w => w.Id == 5).FirstOrDefault();
                    sexModel5.Count = qureySkill.Where(w => w.Id == 5).Count();

                    var sexModel6 = countCountSkillModels.Where(w => w.Id == 6).FirstOrDefault();
                    sexModel6.Count = qureySkill.Where(w => w.Id == 6).Count();

                    var sexModel7 = countCountSkillModels.Where(w => w.Id == 7).FirstOrDefault();
                    sexModel7.Count = qureySkill.Where(w => w.Id == 7).Count();

                    var sexModel8 = countCountSkillModels.Where(w => w.Id == 8).FirstOrDefault();
                    sexModel8.Count = qureySkill.Where(w => w.Id == 8).Count();

                    var sexModel9 = countCountSkillModels.Where(w => w.Id == 9).FirstOrDefault();
                    sexModel9.Count = qureySkill.Where(w => w.Id == 9).Count();

                    var sexModel10 = countCountSkillModels.Where(w => w.Id == 10).FirstOrDefault();
                    sexModel10.Count = qureySkill.Where(w => w.Id == 10).Count();

                    skillResult.Add(sexModel1);
                    skillResult.Add(sexModel2);
                    skillResult.Add(sexModel3);
                    skillResult.Add(sexModel4);
                    skillResult.Add(sexModel5);
                    skillResult.Add(sexModel6);
                    skillResult.Add(sexModel7);
                    skillResult.Add(sexModel8);
                    skillResult.Add(sexModel9);
                    skillResult.Add(sexModel10);
                }
                result = new DashBoardResultModel
                {
                    success = true,
                    status = 200,
                    sexModels = sexResult,
                    areaResidenceModels = areaResidenceResult,
                    skillModels = skillResult,
                    participantTypeModels = participantTypeResult,
                    CountParticipant = countParticipant

                };


            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GeCoutDatatDashBoard : " + ex.Message;
            }
            return result;
        }
        #region Get Data
        public HomePageResultModel GetAllData()
        {
            var result = new HomePageResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var skillModels = new List<CountSkillModel>();
                var participantTypeModels = new List<CountParticipantTypeModel>();
                var areaResidenceModels = new List<CountAreaResidenceModel>();
                var sexModels = new List<CountSexModel>();

                skillModels = _context.tbSkill.Select(s => new CountSkillModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Count = 0
                }).ToList();
                participantTypeModels = _context.tbParticipantType.Select(s => new CountParticipantTypeModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Count = 0
                }).ToList();
                areaResidenceModels = _context.tbAreaResidence.Select(s => new CountAreaResidenceModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Latitude = s.Latitude,
                    Longitude = s.Longitude,
                    Count = 0
                }).ToList();
                sexModels = _context.tbSex.Select(s => new CountSexModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Count = 0
                }).ToList();

                result.skillModels = skillModels;
                result.participantTypeModels = participantTypeModels;
                result.areaResidenceModels = areaResidenceModels;
                result.sexModels = sexModels;


            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลทั้งหมดได้ : GetAllData : " + ex.Message;
            }
            return result;
        }
        #endregion
    }
}
