﻿using ChonburiServer_API.Models;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.SignAuthen;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChonburiServer_API.Services.Interfaces.Staff
{
    public interface IStaffService 
    {
        #region staff
        StaffResultModels GetStaffs(StaffFilterModel model);
        StaffResultModel GetStaffById(int id);
        ResultModel DeleteStaffById(int id);
        ResultModel CrateStaffById(StaffsModel model);
        ResultModel UpdateStaffById(int id , StaffsModel model);

        #endregion

        #region role
        Task<RoleResultModels> GetRoles(RoleFilterModel model);
        RoleResultModel GetRoleById(int id);
        ResultModel DeleteRoleById(int id);
        ResultModel CrateRoleById(RoleModel model);
        ResultModel UpdateRoleById(int id, RoleModel model);
        #endregion


    }
}
