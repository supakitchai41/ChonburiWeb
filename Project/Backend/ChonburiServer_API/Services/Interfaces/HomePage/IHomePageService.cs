﻿using ChonburiServer_API.Models.HomePage;
using ChonburiServer_API.Models.Participant;

namespace ChonburiServer_API.Services.Interfaces.HomePage
{
    public interface IHomePageService
    {
        HomePageResultModel GeCoutDatatHomePage();
        //HomePageResultModel GetAreaResidenceById(int id);
    }
}
