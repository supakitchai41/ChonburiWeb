﻿using ChonburiServer_API.Models;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.Project;
using ChonburiServer_API.Models.SignAuthen;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChonburiServer_API.Services.Interfaces.Staff
{
    public interface IProjectService
    {
        #region Project
        ProjectResultModels GetProjects(ProjectFilterModel model);
        ProjectResultModel GetProjectById(int id);
        ResultModel DeleteProject(int id);
        ResultModel CrateProject(ProjectModel model);
        ResultModel UpdateProject(int id , ProjectModel model);

        #endregion


    }
}
