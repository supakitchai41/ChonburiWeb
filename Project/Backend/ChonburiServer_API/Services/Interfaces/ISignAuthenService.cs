﻿
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Newtonsoft.Json.Linq;
using ChonburiServer_API.DataAccess;
using ChonburiServer_API.Models;
using ChonburiServer_API.Models.SignAuthen;
using System.Collections.Generic;
using ChonburiServer_API.Models.Staff;

namespace ChonburiServer_API.Services.Interfaces
{
    public interface ISignAuthenService
    {
        StaffResultModel EmpAuthen(SingInModel authen);
        #region Token
        string GenerateToken(StaffsModel model);
        #endregion
    }
}
