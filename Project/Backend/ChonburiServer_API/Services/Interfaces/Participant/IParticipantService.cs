﻿using ChonburiServer_API.Models;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.Participant;
using ChonburiServer_API.Models.SignAuthen;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChonburiServer_API.Services.Interfaces.Participant
{
    public interface IParticipantService
    {
        #region  Participant
        ParticipantResultModels Gets(ParticipantFilterModel model);
        ParticipantResultModel GetById(int id);
        ResultModel Delete(int id);
        ResultModel Crate(ParticipantModel model);
        ResultModel Crates(List<ParticipantModel>  model);
        ResultModel Update(int id , ParticipantModel model);
        AllTypeInParticipantModel GetAllTypeInParticipant();
        #endregion


    }
}
