﻿using ChonburiServer_API.Models.DashBoard;
using ChonburiServer_API.Models.HomePage;

namespace ChonburiServer_API.Services.Interfaces.DashBoard
{
    public interface IDashBoardService
    {
        DashBoardResultModel GeCoutDatatDashBoard();
    }
}
