﻿using ChonburiServer_API.Models;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.Project;
using ChonburiServer_API.Services.Interfaces.Staff;

namespace ChonburiServer_API.Services.Project
{
    public class ProjectService : IProjectService
    {
        public ProjectService()
        {

        }

        public ResultModel CrateProject(ProjectModel model)
        {
            throw new System.NotImplementedException();
        }

        public ResultModel DeleteProject(int id)
        {
            throw new System.NotImplementedException();
        }

        public ProjectResultModel GetProjectById(int id)
        {
            throw new System.NotImplementedException();
        }

        public ProjectResultModels GetProjects(ProjectFilterModel model)
        {
            throw new System.NotImplementedException();
        }

        public ResultModel UpdateProject(int id, ProjectModel model)
        {
            throw new System.NotImplementedException();
        }
    }
}
