﻿using ChonburiServer_API.DataAccess.Chonburi.Context;
using ChonburiServer_API.DataAccess.Chonburi.Entity;
using ChonburiServer_API.Models;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.Participant;
using ChonburiServer_API.Models.Staff.Role;
using ChonburiServer_API.Services.Interfaces.Participant;
using System;
using System.Collections.Generic;
using System.Linq;
using DateTime = System.DateTime;

namespace ChonburiServer_API.Services.Participant
{
    public class ParticipantService : IParticipantService
    {
        private readonly ChonburiContext _context;
        private string createBy = "";
        public ParticipantService(ChonburiContext context)
        {
            _context = context;
        }

        public ResultModel Crate(ParticipantModel model)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true
            };
            try
            {
                this.AddUpdate(model);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "Crate" + ex.Message;
            }
            return result;
        }

        public ResultModel Delete(int id)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var query = _context.tbParticipant.Find(id);
                if (query != null)
                {
                    query.IsDeleted = true;
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถลบได้" + ex.Message;
            }
            return result;
        }

        public ParticipantResultModel GetById(int id)
        {
            var result = new ParticipantResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var getDataAll = this.GetAllData();
                var data = new ResponseParticipantModel();
                data = _context.tbParticipant.Where(w => w.Id == id).Select(s => new ResponseParticipantModel
                {
                    Id = s.Id,
                    TitleName = s.TitleName,
                    CustName = s.CustName,
                    CustSurename = s.CustSurename,
                    NickName = s.NickName,
                    Sex = s.Sex,
                    //sexModel = new SexModel
                    //{
                    //    Id = s.Sex,
                    //    Name = getDataAll.sexModels.Where(w => w.Id == s.Sex).Select(s => s.Name).FirstOrDefault()
                    //},
                    OtherSex = s.OtherSex,
                    Age = s.Age,
                    Birthday = s.Birthday.ToString("yyyy-MM-dd HH:mm:ss"),
                    DistrictOfResidence = s.DistrictOfResidence,
                    //areaResidenceModel = new AreaResidenceModel
                    //{
                    //    Id = s.DistrictOfResidence,
                    //    Name = getDataAll.areaResidenceModels.Where(w => w.Id == s.DistrictOfResidence).Select(s => s.Name).FirstOrDefault()
                    //},
                    Idno = s.Idno,
                    Address = s.Address,
                    Email = s.Email,
                    Phone =s.Phone,
                    ParentName = s.ParentName,
                    ParentPhone = s.ParentPhone,
                    Relevance = s.Relevance ,
                    //relevanceModel = new RelevanceModel
                    //{
                    //    Id = s.Relevance,
                    //    Name = getDataAll.relevanceModels.Where(w => w.Id == Convert.ToInt32(s.Relevance)).Select(s => s.Name).FirstOrDefault()
                    //},
                    ParticipantType = s.ParticipantType,
                    //participantTypeModel = new ParticipantTypeModel
                    //{
                    //    Id = s.ParticipantType,
                    //    Name = getDataAll.participantTypeModels.Where(w => w.Id == Convert.ToInt32(s.ParticipantType)).Select(s => s.Name).FirstOrDefault()
                    //},
                    ProfessionalSkill = s.ProfessionalSkill,
                    //skillModel = new SkillModel
                    //{
                    //    Id = Convert.ToInt32(s.ProfessionalSkill),
                    //    Name = getDataAll.skillModels.Where(w => w.Id == Convert.ToInt32(s.ProfessionalSkill)).Select(s => s.Name).FirstOrDefault()
                    //},
                    OtherProfessionalSkill = s.OtherProfessionalSkill,
                    WouldLikeProject = s.WouldLikeProject,
                    ParticipantSkillModels = new List<ParticipantSkillModel>()
                }).FirstOrDefault();

                if (data != null)
                {
                    var participantSkillModels = _context.tbParticipantSkill.Where(w => w.IdParticipant == data.Id).Select(s => new ParticipantSkillModel
                    {
                        IdParticipant = s.IdParticipant,
                        IdSkill = s.IdSkill,
                    }).ToList();
                    if (participantSkillModels.Count > 0)
                    {
                        data.ParticipantSkillModels = participantSkillModels;
                    }
                    result.data = data;

                }
                else
                {
                    result.status = 500;
                    result.success = false;
                    result.message = "ไม่สามารถดึงข้อมูล Participant:";
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลของ Participant By ID :" + ex.Message;
            }
            return result;
        }

        public ParticipantResultModels Gets(ParticipantFilterModel model)
        {
            var result = new ParticipantResultModels
            {
                status = 200,
                success = true,

            };
            try
            {
                var getDataAll = this.GetAllData();
                var data = new List<ResponseParticipantModel>();

                var query = _context.tbParticipant
                 .Where(a => !a.IsDeleted)
                 .AsQueryable();
                if (model.Id !=null )
                    query = query.Where(w => w.Id == model.Id);
                if (model.TitleName != null)
                    query = query.Where(w => w.TitleName == model.TitleName);
                if (model.CustName != null)
                    query = query.Where(w => w.CustName == model.CustName);
                if (model.CustSurename != null)
                    query = query.Where(w => w.CustSurename == model.CustSurename);
                if (model.NickName != null)
                    query = query.Where(w => w.NickName == model.NickName);
                if (model.Sex != null)
                    query = query.Where(w => w.Sex == model.Sex);
                if (model.Age != null)
                    query = query.Where(w => w.Age == model.Age);
                if (model.Birthday != null)
                    query = query.Where(w => w.Birthday == model.Birthday);
                if (model.DistrictOfResidence != null)
                    query = query.Where(w => w.DistrictOfResidence == model.DistrictOfResidence);
                if (model.Idno != null)
                    query = query.Where(w => w.Idno == model.Idno);
                if (model.Email != null)
                    query = query.Where(w => w.Email == model.Email);
                if (model.Phone != null)
                    query = query.Where(w => w.Phone == model.Phone);
                if (model.ParticipantType != null)
                    query = query.Where(w => w.ParticipantType == model.ParticipantType);
                if (model.ProfessionalSkill != null)
                    query = query.Where(w => w.ProfessionalSkill == model.ProfessionalSkill);
                data = query.Select(s => new ResponseParticipantModel
                {
                    Id = s.Id,
                    TitleName = s.TitleName,
                    CustName = s.CustName,
                    CustSurename = s.CustSurename,
                    NickName = s.NickName,
                    Sex = s.Sex,

                    OtherSex = s.OtherSex,
                    Age = s.Age,
                    Birthday = s.Birthday.ToString(("yyyy-MM-dd HH:mm:ss")),
                    DistrictOfResidence = s.DistrictOfResidence,

                    Idno = s.Idno,
                    Address = s.Address,
                    Email = s.Email,
                    Phone = s.Phone,
                    ParentName = s.ParentName,
                    ParentPhone = s.ParentPhone,
                    Relevance = s.Relevance,
                    ParticipantType = s.ParticipantType,
                    ProfessionalSkill = s.ProfessionalSkill,
                    OtherProfessionalSkill = s.OtherProfessionalSkill,
                    WouldLikeProject = s.WouldLikeProject,
                    ParticipantSkillModels = new List<ParticipantSkillModel>()
                }).ToList();

                if (data.Count > 0)
                {
                    result.data = data;

                }
                else
                {
                    result.status = 500;
                    result.success = false;
                    result.message = "ไม่สามารถดึงข้อมูลของพนักงาน:";
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลของพนักงานทั้งหมดได้ :" + ex.Message;
            }
            return result;
        }

        public ResultModel Update(int id, ParticipantModel model)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true
            };
            try
            {
                model.Id = id;
                this.AddUpdate(model);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "Update" + ex.Message;
            }
            return result;
        }
        #region AddUpdate
        private void AddUpdate(ParticipantModel input)
        {

            var entry = input.Id == 0 ? new tbParticipant() : _context.tbParticipant.Find(input.Id);
            if (input.Id == 0)
            {
                entry.IsDeleted = false;
                entry.CreatedDate = DateTime.Now;
                entry.CreatedBy = createBy != "" ? createBy : "System";
                _context.tbParticipant.Add(entry);
            }

            entry.TitleName = input.TitleName;
            entry.CustName = input.CustName;
            entry.CustSurename = input.CustSurename;
            entry.NickName = input.NickName != null ? input.NickName : "";
            entry.Sex = input.Sex;
            entry.OtherSex = input.OtherSex != null ? input.OtherSex : "";
            entry.Age = input.Age;
            entry.Birthday = input.Birthday != null ?  DateTime.Parse(input.Birthday) : DateTime.Now;
            entry.DistrictOfResidence = input.DistrictOfResidence;
            entry.Idno = input.Idno != null ? input.Idno : "";
            entry.Address = input.Address != null ? input.Address : "";
            entry.Email = input.Email != null ? input.Email : "";
            entry.Phone = input.Phone != null ? input.Phone : "";
            entry.ParentName = input.ParentName != null ? input.ParentName : "";
            entry.ParentPhone = input.ParentPhone != null ? input.ParentPhone : "";
            entry.Relevance = input.Relevance;
            entry.ParticipantType = input.ParticipantType;
            entry.ProfessionalSkill = input.ProfessionalSkill;
            entry.OtherProfessionalSkill = input.OtherProfessionalSkill != null ? input.OtherProfessionalSkill : "";
            entry.WouldLikeProject = input.WouldLikeProject != null ? input.WouldLikeProject : "";
            entry.UpdatedBy = createBy != "" ? createBy : "System";
            entry.UpdatedDate = DateTime.Now;
            _context.SaveChanges();
            this.AddUpdateSkill(entry , input);
        }
        #region AddSkill
        private void AddUpdateSkill(tbParticipant tb ,ParticipantModel input)
        {
            try
            {
                if (input.Id > 0)
                {
                    var query = _context.tbParticipantSkill.Where(w => w.IdParticipant == input.Id).ToList();
                    _context.RemoveRange(query);
                }
                if (tb.Id >0 || input.Id > 0)
                {
                    foreach (var item in input.ParticipantSkillModels)
                    {
                        var entry = new tbParticipantSkill();
                        entry.IdParticipant = tb.Id;
                        entry.IdSkill = item.IdSkill;
                        _context.tbParticipantSkill.Add(entry);
                    }
                }
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
           


        }
        #endregion
        #endregion
        #region Get Data
        public ParticipantDataResultModel GetAllData()
        {
            var result = new ParticipantDataResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var skillModels = new List<SkillModel>();
                var participantTypeModels = new List<ParticipantTypeModel>();
                var relevanceModels = new List<RelevanceModel>();
                var areaResidenceModels = new List<AreaResidenceModel>();
                var sexModels = new List<SexModel>();

                skillModels = _context.tbSkill.Select(s => new SkillModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
                participantTypeModels = _context.tbParticipantType.Select(s => new ParticipantTypeModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
                relevanceModels = _context.tbRelevance.Select(s => new RelevanceModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
                areaResidenceModels = _context.tbAreaResidence.Select(s => new AreaResidenceModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
                sexModels = _context.tbSex.Select(s => new SexModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();

                result.skillModels = skillModels;
                result.participantTypeModels = participantTypeModels;
                result.relevanceModels = relevanceModels;
                result.areaResidenceModels = areaResidenceModels;
                result.sexModels = sexModels;


            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลทั้งหมดได้ : GetAllData : " + ex.Message;
            }
            return result;
        }

        public ResultModel Crates(List<ParticipantModel> models)
        {
            createBy = "System Excel";
            var result = new ResultModel()
            {
                status = 200,
                success = true
            };
            try
            {
                foreach (var item in models)
                {
                    this.AddUpdate(item);
                }
            }
            catch(Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "Crates : " + ex.Message;
            }
            return result;
           
        }
        #region AllTypeInParticipant
        public AllTypeInParticipantModel GetAllTypeInParticipant()
        {
            var result = new AllTypeInParticipantModel()
            {
                success = true,
                status = 200,
                areaResidences = new List<AreaResidenceModel>(),
                sexs = new List<SexModel>(),
                skillsl = new List<SkillModel>(),
                relevances = new List<RelevanceModel>(),
                participantTypes = new List<ParticipantTypeModel>()

            };
            try
            {
                var getdata =  this.GetAllData();
                result.skillsl = getdata.skillModels;
                result.sexs = getdata.sexModels;
                result.areaResidences = getdata.areaResidenceModels;
                result.participantTypes = getdata.participantTypeModels;
                result.relevances = getdata.relevanceModels;

            }
            catch(Exception ex)
            {
                result.status = 500;
                result.success = false;
                result.message = "GetAllTypeInParticipant :" + ex.Message;
            }
            return result;
        }
        #endregion



        #endregion
    }
}
