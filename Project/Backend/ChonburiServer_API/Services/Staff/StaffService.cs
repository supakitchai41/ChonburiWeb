﻿
using Microsoft.EntityFrameworkCore;
using ChonburiServer_API.DataAccess.Chonburi.Context;
using ChonburiServer_API.DataAccess.Chonburi.Entity;
using ChonburiServer_API.Models;
using ChonburiServer_API.Models.Filter;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.Models.Staff.Role;
using ChonburiServer_API.Services.Interfaces.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ChonburiServer_API.Services.Staff
{
    public class StaffService : IStaffService
    {
        private readonly ChonburiContext _context;
        public StaffService(ChonburiContext context)
        {
            _context = context;
        }
        #region Role

        public ResultModel DeleteRoleById(int id)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var query = _context.tbRoles.Find(id);
                if (query != null)
                {
                    //  query.Active = 1;
                    _context.Remove(query);
                    _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถลบได้" + ex.Message;
            }
            return result;
        }

       
        
        public RoleResultModel GetRoleById(int id)
        {
            var result = new RoleResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = new RoleModel();
                data = _context.tbRoles.Where(w=>w.Id == id).Select(s => new RoleModel
                {
                    Id = s.Id,
                    FirstName = s.Name
                }).FirstOrDefault();

                if (data != null)
                {
                    result.data = data;

                }
                else
                {
                    result.status = 500;
                    result.success = false;
                    result.message = "ไม่สามารถดึงข้อมูลของพนักงาน:";
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลของพนักงานทั้งหมดได้ :" + ex.Message;
            }
            return result;

        }

        public async Task<RoleResultModels> GetRoles(RoleFilterModel model)
        {
            var result = new RoleResultModels
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = new List<RoleModel> ();

                var query = _context.tbRoles
                  // .Where(a => a.Active == 0)
                  .AsQueryable();
                if (model.RoleId > 0)
                    query = query.Where(w => w.Id == model.RoleId);
                if (model.RoleName != null)
                    query = query.Where(w => w.Name == model.RoleName);

                 data = query.Select(s => new RoleModel
                {
                    Id = s.Id,
                    FirstName = s.Name
                }).ToList();

                if (data.Count > 0)
                {
                    result.data = data;

                }
                else
                {
                    result.status = 500;
                    result.success = false;
                    result.message = "ไม่สามารถดึงข้อมูลต่ำแหน่ง:";
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลของพนักงานทั้งหมดได้ :" + ex.Message;
            }
            return result;
        }
        public ResultModel UpdateRoleById(int id, RoleModel model)
        {
            var result = new ResultModel
            {
                status =200,
                success = true
            };
            try
            {
                model.Id = id;
                this.AddOrUpdateRole(model);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "UpdateRoleById" + ex.Message;
            }
            return result;
        }

        public ResultModel CrateRoleById(RoleModel model)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true
            };
            try
            {
                this.AddOrUpdateRole(model);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "CrateRoleById" + ex.Message;
            }
            return result;
        }
        #endregion

        #region staff

        public ResultModel CrateStaffById(StaffsModel model)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true
            };
            try
            {
                this.AddOrUpdateStaff(model);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "CrateStaffById" + ex.Message;
            }
            return result;
        }

        public ResultModel UpdateStaffById(int id, StaffsModel model)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true
            };
            try
            {
                model.Id = id;
                this.AddOrUpdateStaff(model);
            }
            catch (Exception ex)
            {
                result.success = false;
                result.status = 500;
                result.message = "CrateStaffById" + ex.Message;
            }
            return result;
        }
        public ResultModel DeleteStaffById(int id)
        {
            var result = new ResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var query = _context.tbStaff.Find(id);
                if (query != null)
                {
                    _context.Remove(query);
                     _context.SaveChanges();
                }

            }
            catch(Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถลบได้" + ex.Message;
            }
            return result;
        }
        public StaffResultModel GetStaffById(int id)
        {
            var result = new StaffResultModel
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = new StaffsModel();
                var queryRoles = _context.tbRoles.Select(s => new RoleModel
                {
                    Id = s.Id,
                    FirstName = s.Name
                }).ToList();

                var tbStaff = _context.tbStaff.Where(w=>w.Id == id).Select(s => new StaffsModel
                {
                    Id = s.Id,
                    RoleId = s.RoleId,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    Email = s.Email,
                    Phone = s.Phone,
                    //role = new RoleModel
                    //{
                    //    Id = s.Id,
                    //    FirstName = queryRoles.Where(w => w.Id == s.Id).Select(ss => ss.FirstName).FirstOrDefault()
                    //}
                }).FirstOrDefault();
                if (tbStaff != null)
                {
                    if(tbStaff.RoleId > 0)
                    {
                        var roleName = queryRoles.Where(w => w.Id == tbStaff.RoleId).Select(s => s.FirstName).FirstOrDefault();

                        tbStaff.role = new RoleModel
                        {
                            Id = data.RoleId,
                            FirstName = roleName

                        };
                    }
                    result.data = tbStaff;

                }
                else
                {
                    result.status = 500;
                    result.success = false;
                    result.message = "ไม่สามารถดึงข้อมูลของพนักงาน:";
                }

            }
            catch (Exception ex)
            {
                result.status = 500;
                result.success = true;
                result.message = "ไม่สามารถดึงข้อมูลของพนักงานทั้งหมดได้ :" + ex.Message;
            }
            return result;
        }

        public StaffResultModels GetStaffs(StaffFilterModel model)
        {
            var result = new StaffResultModels
            {
                status = 200,
                success = true,

            };
            try
            {
                var data = new List<StaffsModel>();

                var query = _context.tbStaff
                   //.Where(a => a.Deleted == 0)
                   .AsQueryable();
                if (model.StaffName != null)
                    query = query.Where(w => w.FirstName == model.StaffName || w.LastName == model.StaffName || w.FirstName + " " + w.LastName == model.StaffName);
                if (model.RoleId != null)
                    query = query.Where(w => w.RoleId == model.RoleId);
                query = query.OrderBy(o => o.Id);
                
                if(query.ToList().Count > 0)
                {
                    var queryRoles = _context.tbRoles.Select(s=> new RoleModel
                    {
                        Id = s.Id,
                        FirstName = s.Name
                    }).ToList();
                    data = query.Select(s => new StaffsModel
                    {
                       Id = s.Id,
                       RoleId = s.RoleId,
                       FirstName = s.FirstName,
                       LastName = s.LastName,
                       Email = s.Email,
                       Phone = s.Phone,
                    }).ToList();
                    if (data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            if (item.RoleId > 0)
                            {
                                var roleName = queryRoles.Where(w => w.Id == item.RoleId).Select(s => s.FirstName).FirstOrDefault();
                                item.role = new RoleModel
                                {
                                    Id = item.RoleId,
                                    FirstName = roleName,
                                };
                            }
                        }
                        result.data = data;
                    }
                    else
                    {
                        result.status = 500;
                        result.success = false;
                        result.message = "ไม่สามารถดึงข้อมูลของพนักงานทั้งหมดได้";
                    }
                }

            }
            catch (Exception ex) 
            {
                result.status = 500;
                result.success = false;
                result.message =  "ไม่สามารถดึงข้อมูลของพนักงานทั้งหมดได้ :" + ex.Message;
            }
            return result;
        }
        #endregion

        #region AddUpdate

        private void  AddOrUpdateRole (RoleModel input)
        {
            var entry = input.Id == 0 ? new tbRoles() : _context.tbRoles.Find(input.Id);
            if (input.Id == 0)
            {
                //entry.Active = 0;
                _context.tbRoles.Add(entry);
            }

            entry.Name = input.FirstName;
             _context.SaveChanges();
        }

        private void AddOrUpdateStaff(StaffsModel input)
        {
            var entry = input.Id == 0 ? new tbStaffs() : _context.tbStaff.Find(input.Id);
            if (input.Id == 0)
            {
               // entry.Deleted = 0;
                entry.CreatedBy = "System";
                entry.CreatedDate = DateTime.Now;
                _context.tbStaff.Add(entry);
            }

            entry.FirstName = input.FirstName;
            entry.LastName = input.LastName;
            entry.Phone = input.Phone;
            entry.Email = input.Email;
            entry.Password = input.Password;
            entry.RoleId = input.RoleId;
            entry.UpdatedBy = "system";
            entry.UpdatedDate = DateTime.Now;
            _context.SaveChanges();
        }

        







        #endregion

    }
}
