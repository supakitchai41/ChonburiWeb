﻿using System;
using System.Linq;
using System.Data;
using ChonburiServer_API.Services.Interfaces;
using System.IO;
using ChonburiServer_API.Models.SignAuthen;
using ChonburiServer_API.Services.Configure;
using Microsoft.Extensions.Configuration;
using System.Collections;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using ChonburiServer_API.Models.Staff;
using ChonburiServer_API.DataAccess.Chonburi.Context;

namespace ChonburiServer_API.Services
{
    public class SignAuthenService : ISignAuthenService
    {

        private readonly IConfiguration _configuration;
        private readonly ChonburiContext _context;

        public SignAuthenService(IConfiguration configuration , ChonburiContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        #region Authen
        public StaffResultModel EmpAuthen(SingInModel authen)
        {
            StaffResultModel staff = new StaffResultModel
            {
                status = 200,
                success = true,
                 
            };
            try
            {
                var data = _context.tbStaff.Where(w => w.Email == authen.empId && w.Password == authen.password).Select(s=> new StaffsModel
                {
                    Id = s.Id,
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    RoleId = s.RoleId,
                    Email = s.Email,
                }).FirstOrDefault();
                if (data != null)
                {
                    staff.data = data;
                }
                else
                {
                    staff.status = 500;
                    staff.success = false;
                    staff.message = "Login failed : ไม่มีข้อมูล ";
                }
            }
            catch (Exception ex)
            {
                staff.status = 500;
                staff.success = false;
                staff.message = "Login failed : " + ex.Message;
            }
            return staff;
        }

        #endregion

        #region Token
        public string GenerateToken(StaffsModel model)
        {
            try
            {
                if (model != null)
                {
                    GenTokenModel genTokenModel = new GenTokenModel
                    {
                        name = model.FirstName + " " + model.LastName,
                        empId = model.Email,
                        securityKey = _configuration["Jwt:Key"],
                        issuer = _configuration["Jwt:Issuer"],
                        expiresMinutes = Convert.ToInt32(_configuration["Jwt:expiresMinutes"])
                    };
                    var resultReturn = CreateJwtSecurityToken(genTokenModel);

                    string token = new JwtSecurityTokenHandler().WriteToken(resultReturn);
                    return token;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private JwtSecurityToken CreateJwtSecurityToken(GenTokenModel genTokenModel)
        {
            try
            {
                var claims = new[] {
                new Claim(ClaimTypes.UserData, genTokenModel.empId),
                new Claim(ClaimTypes.Name, genTokenModel.name),
                new Claim(ClaimTypes.Hash, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, TimeSpan.FromMinutes(genTokenModel.expiresMinutes).ToString())
            };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(genTokenModel.securityKey));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(genTokenModel.issuer,
                  genTokenModel.issuer,
                  claims,
                  expires: DateTime.Now.AddMinutes(genTokenModel.expiresMinutes),
                  signingCredentials: creds);

                return token;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
