﻿using Microsoft.EntityFrameworkCore;
using ChonburiServer_API.DataAccess.Chonburi.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChonburiServer_API.DataAccess.Chonburi.Interfaces
{
    public interface IChonburiDataSets
    {
        DbSet<tbRoles> tbRoles { get; }
        DbSet<tbStaffs> tbStaffs { get; }
    }
}
