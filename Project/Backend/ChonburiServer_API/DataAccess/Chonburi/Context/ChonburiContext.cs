﻿using Microsoft.EntityFrameworkCore;
using ChonburiServer_API.DataAccess.Chonburi.Entity;
using ChonburiServer_API.DataAccess.Chonburi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChonburiServer_API.DataAccess.Chonburi.Context
{
    public class ChonburiContext : DbContext
    {
        public DbSet<tbRoles> tbRoles => Set<tbRoles>();
        public DbSet<tbStaffs> tbStaff => Set<tbStaffs>();
        public DbSet<tbSkill> tbSkill => Set<tbSkill>();
        public DbSet<tbParticipantType> tbParticipantType => Set<tbParticipantType>();
        public DbSet<tbRelevance> tbRelevance => Set<tbRelevance>();
        public DbSet<tbAreaResidence> tbAreaResidence => Set<tbAreaResidence>();
        public DbSet<tbSex> tbSex => Set<tbSex>();
        public DbSet<tbParticipant> tbParticipant => Set<tbParticipant>(); 
        public DbSet<tbParticipantSkill> tbParticipantSkill => Set<tbParticipantSkill>();

        public ChonburiContext(DbContextOptions<ChonburiContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region role
            var roleBuilder = builder.Entity<tbRoles>();
            roleBuilder.ToTable("tbRoles", "dbo");
            roleBuilder.HasKey(x => new { x.Id });
            #endregion

            #region staff
            var stassBuilder = builder.Entity<tbStaffs>();
            stassBuilder.ToTable("tbStaff", "dbo");
            stassBuilder.HasKey(x => new { x.Id });
            #endregion

            #region tbSkill
            var skillBuilder = builder.Entity<tbSkill>();
            skillBuilder.ToTable("tbSkill", "dbo");
            skillBuilder.HasKey(x => new { x.Id });
            #endregion

            #region tbParticipantType
            var participantTypeBuilder = builder.Entity<tbParticipantType>();
            participantTypeBuilder.ToTable("tbParticipantType", "dbo");
            participantTypeBuilder.HasKey(x => new { x.Id });
            #endregion

            #region tbRelevance
            var relevanceBuilder = builder.Entity<tbRelevance>();
            relevanceBuilder.ToTable("tbRelevance", "dbo");
            relevanceBuilder.HasKey(x => new { x.Id });
            #endregion

            #region tbAreaResidence
            var areaResidenceBuilder = builder.Entity<tbAreaResidence>();
            areaResidenceBuilder.ToTable("tbAreaResidence", "dbo");
            areaResidenceBuilder.HasKey(x => new { x.Id });
            #endregion

            #region tbSex
            var sexBuilder = builder.Entity<tbSex>();
            sexBuilder.ToTable("tbSex", "dbo");
            sexBuilder.HasKey(x => new { x.Id });
            #endregion

            #region tbParticipantSkill
            var ParticipantSkill = builder.Entity<tbParticipantSkill>();
            ParticipantSkill.ToTable("tbParticipantSkill", "dbo");
            ParticipantSkill.HasKey(x => new { x.Id });
            #endregion

        }
    }
}
