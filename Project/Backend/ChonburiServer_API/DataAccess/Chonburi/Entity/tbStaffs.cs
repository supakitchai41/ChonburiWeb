﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChonburiServer_API.DataAccess.Chonburi.Entity
{
    public class tbStaffs
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("FirstName")]
        public string FirstName { get; set; }
        [Column("LastName")]
        public string LastName { get; set; }
        [Column("Phone")]
        public string Phone { get; set; }

        [Column("Email")]
        public string Email { get; set; }
        [Column("Password")]
        public string Password { get; set; }
        [Column("RoleId")]
        public int RoleId { get; set; }

        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Column("CreatedBy")]
        public string CreatedBy { get; set; }
        [Column("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Column("UpdatedBy")]
        public string UpdatedBy { get; set; }
    }
}
