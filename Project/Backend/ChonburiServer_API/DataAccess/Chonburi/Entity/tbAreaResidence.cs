﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ChonburiServer_API.DataAccess.Chonburi.Entity
{
    public class tbAreaResidence
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        public string Name { get; set; }
        [Column("Latitude")]
        public string Latitude { get; set; }
        [Column("Longitude")]
        public string Longitude { get; set; }
    }
}
