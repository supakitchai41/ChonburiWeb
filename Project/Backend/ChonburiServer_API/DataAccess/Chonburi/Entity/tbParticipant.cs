﻿using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace ChonburiServer_API.DataAccess.Chonburi.Entity
{
    public class tbParticipant
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("TitleName")]
        public string TitleName { get; set; }
        [Column("CustName")]
        public string CustName { get; set; }
        [Column("CustSurename")]
        public string CustSurename { get; set; }

        [Column("NickName")]
        public string NickName { get; set; }

        [Column("Sex")]
        public int Sex { get; set; }
        [Column("OtherSex")]
        public string OtherSex { get; set; }

        [Column("Age")]
        public int Age { get; set; }
        [Column("Birthday")]
        public DateTime Birthday { get; set; }
        [Column("DistrictOfResidence")]
        public int DistrictOfResidence { get; set; }

        [Column("Idno")]
        public string Idno { get; set; }

        [Column("Address")]
        public string Address { get; set; }
        [Column("Email")]
        public string Email { get; set; }


        [Column("Phone")]
        public string Phone { get; set; }
        [Column("ParentName")]
        public string ParentName { get; set; }
        [Column("ParentPhone")]
        public string ParentPhone { get; set; }

        [Column("Relevance")]
        public int Relevance { get; set; }

        [Column("ParticipantType")]
        public int ParticipantType { get; set; }
        [Column("ProfessionalSkill")]
        public int ProfessionalSkill { get; set; }

        [Column("OtherProfessionalSkill")]
        public string OtherProfessionalSkill { get; set; }
       
        [Column("WouldLikeProject")]
        public string WouldLikeProject { get; set; }

        [Column("IsDeleted")]
        public bool IsDeleted { get; set; }
        [Column("CreatedDate")]
        public DateTime CreatedDate { get; set; }
        [Column("CreatedBy")]
        public string CreatedBy { get; set; }
        [Column("UpdatedDate")]
        public DateTime UpdatedDate { get; set; }
        [Column("UpdatedBy")]
        public string UpdatedBy { get; set; }
    }
}
