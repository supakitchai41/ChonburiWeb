﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ChonburiServer_API.DataAccess.Chonburi.Entity
{
    public class tbSkill
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        public string Name { get; set; }
    }
}
