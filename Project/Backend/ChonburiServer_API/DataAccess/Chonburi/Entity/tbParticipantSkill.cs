﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ChonburiServer_API.DataAccess.Chonburi.Entity
{
    public class tbParticipantSkill
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("IdParticipant")]
        public int IdParticipant { get; set; }
        [Column("IdSkill")]
        public int IdSkill { get; set; }
    }
}
