﻿using ChonburiServer_API.Models.Staff;
using System.Collections.Generic;

namespace ChonburiServer_API.Models.Project
{
    public class ProjectModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }

    }
    public class ProjectResultModels : ResultModel
    {
        public List<ProjectModel> data { get; set; }

    }
    public class ProjectResultModel : ResultModel
    {
        public ProjectModel data { get; set; }

    }
}
