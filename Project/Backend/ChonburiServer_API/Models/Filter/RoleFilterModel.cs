﻿namespace ChonburiServer_API.Models.Filter
{
    public class RoleFilterModel
    {
        public int? RoleId { get; set; }
        public string? RoleName { get; set; }
    }
}
