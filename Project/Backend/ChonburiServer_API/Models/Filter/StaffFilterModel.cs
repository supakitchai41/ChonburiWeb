﻿namespace ChonburiServer_API.Models.Filter
{
    public class StaffFilterModel
    {
        public int? StaffId { get; set; }
        public string? StaffName { get; set; }
        public string? RoleName { get; set; }
        public int? RoleId { get; set; }
    }
}
