﻿namespace ChonburiServer_API.Models.Filter
{
    public class ProjectFilterModel
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
