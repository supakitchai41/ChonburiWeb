﻿using System;

namespace ChonburiServer_API.Models.Filter
{
    public class ParticipantFilterModel
    {
        public int? Id { get; set; }
        public string? TitleName { get; set; }
        public string? CustName { get; set; }
        public string? CustSurename { get; set; }
        public string? NickName { get; set; }
        public int? Sex { get; set; }
        public int? Age { get; set; }
        public DateTime? Birthday { get; set; }
        public int? DistrictOfResidence { get; set; }
        public string? Idno { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public int? ParticipantType { get; set; }
        public int? ProfessionalSkill { get; set; }

    }
}
