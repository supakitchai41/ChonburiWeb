﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChonburiServer_API.Models
{
    public class ResultModel
    {
        public int status { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
    }
    
}
