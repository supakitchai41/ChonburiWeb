﻿namespace ChonburiServer_API.Models.BuildToken
{
    public class BuildTokenCheckSerialModel
    {
        public string serialNumber { get; set; }
        public string securityKey { get; set; }
        public string issuer { get; set; }
        public int expiresMinutes { get; set; }
        public string urlCheckSerial     { get; set; }
    }
}
