﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ChonburiServer_API.Models.Participant
{
    public class ParticipantSkillModel
    {
        public int IdParticipant { get; set; }
        public int IdSkill { get; set; }
    }
}
