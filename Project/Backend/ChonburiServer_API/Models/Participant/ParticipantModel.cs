﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using ChonburiServer_API.Models.Staff;
using System.Collections.Generic;
using ChonburiServer_API.Models.Participant;

namespace ChonburiServer_API.Models.Participant
{
    public class ParticipantModel
    {

        public int Id { get; set; }
        public string TitleName { get; set; }
        public string CustName { get; set; }
        public string CustSurename { get; set; }
        public string NickName { get; set; }
        public int Sex { get; set; }
        public string OtherSex { get; set; }
        public int Age { get; set; }
        public string Birthday { get; set; }
        public int DistrictOfResidence { get; set; }
        public string Idno { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }

        public string ParentName { get; set; }

        public string ParentPhone { get; set; }
        public int Relevance { get; set; }
        public int ParticipantType { get; set; }
        public int ProfessionalSkill { get; set; }
        public string OtherProfessionalSkill { get; set; }
        public string WouldLikeProject { get; set; }
        public List<ParticipantSkillModel>? ParticipantSkillModels { get; set; }


    }
    public class SkillModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ParticipantTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class RelevanceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class AreaResidenceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
    public class SexModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ParticipantResultModels : ResultModel
    {
        public List<ResponseParticipantModel> data { get; set; }

    }
    public class ParticipantResultModel : ResultModel
    {
        public ResponseParticipantModel data { get; set; }

    }
    public class ParticipantDataResultModel : ResultModel
    {
        public List<SkillModel> skillModels { get; set; }
        public List<ParticipantTypeModel> participantTypeModels { get; set; }
        public List<RelevanceModel> relevanceModels { get; set; }
        public List<AreaResidenceModel> areaResidenceModels { get; set; }
        public List<SexModel> sexModels { get; set; }

    }
    public class ResponseParticipantModel : ParticipantModel
    {
        public SkillModel skillModel { get; set; }
        public ParticipantTypeModel participantTypeModel { get; set; }
        public RelevanceModel relevanceModel { get; set; }
        public AreaResidenceModel areaResidenceModel { get; set; }
        public SexModel sexModel { get; set; }
    }
    public class CountIdParticipantModel
    {
        public int Sex { get; set; }
        public int DistrictOfResidence { get; set; }
        public int ParticipantType { get; set; }
        public int ProfessionalSkill { get; set; }
      
    }
    public class AllTypeInParticipantModel : ResultModel
    {
        public List<SkillModel>  skillsl { get; set; }
        public List<ParticipantTypeModel>  participantTypes { get; set; }
        public List<RelevanceModel>  relevances { get; set; }
        public List<AreaResidenceModel>  areaResidences { get; set; }
        public List<SexModel>  sexs { get; set; }
    }

}
