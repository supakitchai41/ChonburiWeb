﻿using ChonburiServer_API.Models.Participant;
using System.Collections.Generic;

namespace ChonburiServer_API.Models.HomePage
{
    public class HomePageModel
    {
        
    }
    public class CountSkillModel : SkillModel
    {
        public int Count { get; set; }

    }
    public class CountParticipantTypeModel : ParticipantTypeModel
    {
        public int Count { get; set; }

    }
    public class CountAreaResidenceModel : AreaResidenceModel
    {
        public int Count { get; set; }

    }
    public class CountSexModel : SexModel
    {
        public int Count { get; set; }
    }
    public class HomePageResultModel : ResultModel
    {
        public List<CountSexModel> sexModels { get; set; }
        public List<CountParticipantTypeModel> participantTypeModels { get; set; }
        public List<CountAreaResidenceModel> areaResidenceModels { get; set; }
        public List<CountSkillModel> skillModels  { get; set; }
    }
    public class IdModel
    {
        public int Id { get; set; }
    }
}
