﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChonburiServer_API.Models.SignAuthen
{
    public class GetAuthenModel
    {
        public string jdCode { get; set; }
        public string empId { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string titleEn { get; set; }
        public string firstNameEn { get; set; }
        public string lastNameEn { get; set; }
        public string timeout { get; set; }
        public string timeExcecute { get; set; }
        public string aesKey { get; set; }
        public string iksn { get; set; }
        public string ipek { get; set; }
        public string agentCode { get; set; }
        public string activity { get; set; }
        public TokenModel tokenModel { get; set; }
    }
}
