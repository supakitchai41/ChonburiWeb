﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChonburiServer_API.Models.SignAuthen
{
    public class SingInModel
    {
        public string empId { get; set; }

        public string password { get; set; }
    }
    public class GenTokenModel
    {
        public string name { get; set; }
        public string role {  get; set; }
        public string empId { get; set; }
        public string securityKey { get; set; }
        public string issuer { get; set; }
        public int expiresMinutes { get; set; }
    }
}
