﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChonburiServer_API.Models.Staff.Role
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
    public class RoleResultModels : ResultModel
    {
        public List<RoleModel> data { get; set; }

    }
    public class RoleResultModel : ResultModel
    {
        public RoleModel data { get; set; }

    }
}
