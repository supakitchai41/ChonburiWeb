﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using ChonburiServer_API.Models.Staff.Role;
using System.Collections.Generic;

namespace ChonburiServer_API.Models.Staff
{
    public class StaffsModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public bool IsDeleted { get; set; }
        public RoleModel? role {get; set; }

    }
    public class StaffResultModels : ResultModel
    {
        public List<StaffsModel> data { get; set; }

    }
    public class StaffResultModel : ResultModel
    {
        public StaffsModel data { get; set; }

    }
}
