﻿using ChonburiServer_API.Models.HomePage;

namespace ChonburiServer_API.Models.DashBoard
{
    public class DashBoardResultModel : HomePageResultModel
    {
        public int CountParticipant { get; set; }
    }
}
