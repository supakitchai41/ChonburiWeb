import { Component, OnInit } from "@angular/core";
import { SingInService } from "../service-api/singin.service";
import { Router } from "@angular/router";
import { AuthService } from "../service/auth.service";
import {
    StaffProfile,
    StaffsModel,
    SingInModel,
    StaffResultModel,
} from "../model-api/staffs.model";
import { MessageService } from "primeng/api";
@Component({
    selector: "app-login",
    providers: [MessageService],
    templateUrl: "./app.login.component.html",
})
export class AppLoginComponent implements OnInit {
    inputSignIn: SingInModel = {} as SingInModel;
    profile: StaffProfile = {} as StaffProfile;
    staff: StaffsModel = {} as StaffsModel;
    resStaff: StaffResultModel = {} as StaffResultModel;
    constructor(
        private signInService: SingInService,
        private messageService: MessageService,
        private router: Router,
        private authService: AuthService
    ) {}

    ngOnInit() {}

    login() {
        this.signInService.signIn(this.inputSignIn).subscribe((res) => {
            //เก็บข้อมูลลง localStorage
            if (res.status == 200) {
                this.resStaff = res;
                this.staff = this.resStaff.data;
                this.profile.firstName = this.staff.firstName;
                this.profile.lastName = this.staff.lastName;
                this.profile.email = this.staff.email;
                this.profile.token = res.message;
                this.authService.sentToken(this.profile);
                this.messageService.add({
                    severity: "success",
                    summary: "Login",
                    detail: "Login success.",
                });
                const token = this.authService.getToken();
                console.log(token);
                if (token) {
                    this.router.navigate(["/backend"]);
                } else {
                    this.messageService.add({
                        severity: "warn",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "เข้าสู่ระบบไม่สำเร็จ",
                    });
                }
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "Login",
                    detail: "Login failed",
                });
            }
        });
    }
}
