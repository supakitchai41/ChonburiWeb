import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StaffProfile } from '../model-api/staffs.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data ={}as StaffProfile;
  constructor(private route: Router) { }

  sentToken(token:any){
    localStorage.setItem("LoggedInFirstName", token['firstName']);
    localStorage.setItem("LoggedInLastName", token['lastName']);
    localStorage.setItem("LoggedInEmail", token['email']);
    localStorage.setItem("LoggedInToken", token['token']);

  }
  getUser(){
    this.data.email = localStorage.getItem("LoggedInEmail") ?? '';
    this.data.firstName = localStorage.getItem("LoggedInFirstName") ?? '';
    this.data.lastName = localStorage.getItem("LoggedInLastName") ?? '';
    return this.data
  }
  getToken(){
    return localStorage.getItem("LoggedInToken");
  }
  islogin(){
    return this.getToken() !== null;
  }
  logout(){
    localStorage.removeItem("LoggedInFirstName");
    localStorage.removeItem("LoggedInLastName");
    localStorage.removeItem("LoggedInEmail");
    localStorage.removeItem("LoggedInToken");
    this.route.navigate['/login']
  }

}
