import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestGetComponent } from './request-get/request-get.component';



@NgModule({
  declarations: [
    RequestGetComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RequestGetComponent
  ]
})
export class RequestModule { }
