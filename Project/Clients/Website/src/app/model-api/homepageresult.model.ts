import { ResultModel } from './result.model';
import { CountSexModel } from './sex.model';
import { CountSkillModel } from './skill.model';
import { CountParticipantTypeModel } from './participanttype.model';
import { CountAreaResidenceModel } from './arearesidence.model';
export interface HomePageResultModel extends ResultModel{
    sexModels:Array<CountSexModel>,
    participantTypeModels:Array<CountParticipantTypeModel>,
    areaResidenceModels:Array<CountAreaResidenceModel>,
    skillModels:Array<CountSkillModel>,
}