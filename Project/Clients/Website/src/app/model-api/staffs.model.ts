import { ResultModel } from "./result.model"
import { RoleModel } from "./role.model"
export interface    StaffsModel{
    id: number,
    firstName: string,
    lastName: string,
    phone: string
    email: string
    password: string
    roleId: number
    isDeleted: boolean
    role?: RoleModel
}
export interface StaffResultModels extends ResultModel{
   data: Array<StaffsModel>
}
export interface StaffResultModel extends ResultModel{
    data: StaffsModel
 }
 export interface StaffFilterModel{
    id?: number,
    staffName: string
    roleName: string
    roleId: number
}
export interface SingInModel{
    empId: string
    password: string
}
export interface StaffProfile{
    firstName: string,
    lastName: string,
    email: string,
    token: string
}

