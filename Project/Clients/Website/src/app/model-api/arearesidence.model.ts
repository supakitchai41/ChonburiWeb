export interface AreaResidenceModel{
    id: number,
    name: string,
    latitude: string,
    longitude: string,

}
export interface CountAreaResidenceModel extends AreaResidenceModel{
    count:number
}