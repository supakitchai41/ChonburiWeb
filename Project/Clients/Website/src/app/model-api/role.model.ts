import { ResultModel } from "./result.model";

export interface RoleModel{
    id: number,
    firstName: string
}
export interface RoleResultModels extends ResultModel{
    data: Array<RoleModel>
}
export interface RoleResultModel extends ResultModel{
    data: RoleModel
}
export interface RoleFilterModel{
    id: number,
    firstName: string
}