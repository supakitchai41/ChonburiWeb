import { ResultModel } from "./result.model";
import { ParticipantSkillModel } from "./participantSkill.model";
import { SkillModel } from "./skill.model";
import { SexModel } from "./sex.model";
import { AreaResidenceModel } from "./arearesidence.model";
import { RelevanceModel } from "./relevance.model";
import { ParticipantTypeModel } from "./participanttype.model";
export interface ParticipantModel{
    id: number,
    titleName: string,
    custName: string,
    custSurename : string,
    nickName: string,
    sex: number,
    otherSex: string,
    age: number,
    birthday: string,
    districtOfResidence: number,
    idno: string,
    address: string,
    email: string,
    phone: string,
    parentName: string,
    parentPhone: string,
    relevance: number,
    participantType: number,
    professionalSkill: number,
    otherProfessionalSkill: string,
    wouldLikeProject: string,
    participantSkillModels: Array<ParticipantSkillModel>
}
export interface ParticipantFilterModel{
    id: number,
    titleName?: string,
    custName?: string,
    sustSurename?: string,
    nickName?: string,
    sex?: number,
    age?: number,
    birthday?: string,
    districtOfResidence?: number,
    idno?: string,
    email?: string,
    phone?: string,
    participantType?: number,
    professionalSkill?: number,
}
export interface ParticipantResultModels extends  ResultModel{
    data: Array<ParticipantModel>,
}
export interface ParticipantResultModel extends  ResultModel{
    data: ParticipantModel,
}
export interface AllTypeInParticipantModel extends  ResultModel{
    skillsl: Array<SkillModel>,
    participantTypes: Array<ParticipantTypeModel>,
    relevances: Array<RelevanceModel>,
    areaResidences: Array<AreaResidenceModel>,
    sexs: Array<SexModel>,
}
