export interface SkillModel{
    id: number,
    name: string
}
export interface CountSkillModel extends SkillModel{
    count: number,
}