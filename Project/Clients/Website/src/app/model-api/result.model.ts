export interface ResultModel{
    status: number
    success: boolean
    message: string
}