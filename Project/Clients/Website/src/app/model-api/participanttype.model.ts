export interface ParticipantTypeModel{
    id: number,
    name: string
}
export interface CountParticipantTypeModel extends  ParticipantTypeModel{
    count: number,
}