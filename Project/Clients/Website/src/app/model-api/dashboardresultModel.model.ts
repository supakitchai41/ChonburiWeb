import { HomePageResultModel } from "./homepageresult.model";
export interface DashBoardResultModel extends HomePageResultModel{
    countParticipant : number
}