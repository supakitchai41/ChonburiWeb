export interface SexModel{
    id: number,
    name: string
}
export interface CountSexModel extends SexModel{
    count: number
}