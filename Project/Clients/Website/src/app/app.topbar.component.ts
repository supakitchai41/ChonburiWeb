import { Component , OnInit } from '@angular/core';
import { AppMainComponent} from './app.main.component';
import { AuthService } from './service/auth.service';
import { StaffProfile } from './model-api/staffs.model';
@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent implements OnInit{

  data = {} as StaffProfile
    constructor(public app: AppMainComponent, private authService:AuthService) {}
    
    ngOnInit() {
      const staff = this.authService.getUser();
      this.data.firstName = staff.firstName;
      this.data.lastName = staff.lastName;
    }
    logout(){
      this.authService.logout();
    }
  
    
}
