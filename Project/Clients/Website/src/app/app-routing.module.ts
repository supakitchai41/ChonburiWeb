import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { DashboardDemoComponent } from "./demo/view/dashboarddemo.component";
import { AppMainComponent } from "./app.main.component";
import { AppNotfoundComponent } from "./pages/app.notfound.component";
import { AppErrorComponent } from "./pages/app.error.component";
import { AppAccessdeniedComponent } from "./pages/app.accessdenied.component";
import { AppLoginComponent } from "./pages/app.login.component";

//หน้าที่ทำเอง
import { MainParticipantComponent } from "./demo/view/participant/main-participant/main-participant.component";
import { ManageParticipantComponent } from "./demo/view/participant/manage-participant/manage-participant.component";
import { MainAdminComponent } from "./demo/view/admin/main-admin/main-admin.component";
import { ManageAdminComponent } from "./demo/view/admin/manage-admin/manage-admin.component";
import { MainRoleComponent } from "./demo/view/role/main-role/main-role.component";
import { MainProjectComponent } from "./demo/view/project/main-project/main-project.component";
import { ManageProjectComponent } from "./demo/view/project/manage-project/manage-project.component";
//front end
import { FrontendLayoutComponent } from "./demo/view/layouts/frontend-layout/frontend-layout.component";
import { HomeComponent } from "./demo/view/layouts/home/home.component";
import { ContactComponent } from "./demo/view/layouts/contact/contact.component";
import { AboutComponent } from "./demo/view/layouts/about/about.component";
import { OverViewComponent } from "./demo/view/layouts/overview/overview.component";
import { TimelineComponent } from "./demo/view/layouts/timeline/timeline.component";
import { EditAdminComponent } from "./demo/view/admin/edit-admin/edit-admin.component";
import { EditRoleComponent } from "./demo/view/role/main-role/edit-role/edit-role.component";
import { EditParticipantComponent } from "./demo/view/participant/edit-participant/edit-participant/edit-participant.component";
import { AddRoleComponent } from "./demo/view/role/main-role/add-role/add-role/add-role.component";
import { AuthGuard } from "./auth/auth.guard";
import { ViewParticipantComponent } from "./demo/view/participant/view-participant/view-participant/view-participant.component";
@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                // Route สำหรับเรียกหน้า Frontend Layout
                {
                    path: "",
                    component: FrontendLayoutComponent,
                    children: [
                        {
                            path: "",
                            component: HomeComponent,
                            pathMatch: "full",
                            data: {
                                title: "หน้าหลัก",
                                keywords: "หน้าหลัก, Store, Angular, App",
                                description: "This is home store angular app",
                            },
                        },
                        {
                            path: "about",
                            component: AboutComponent,
                            data: {
                                title: "เกี่ยวกับเรา",
                                keywords: "เกี่ยวกับเรา, Store, Angular, App",
                                description: "This is about store angular app",
                            },
                        },
                        {
                            path: "overview",
                            component: OverViewComponent,
                            data: {
                                title: "ภาพรวมโครงการ",
                                keywords: "ภาพรวมโครงการ, Store, Angular, App",
                                description:
                                    "This is overview store angular app",
                            },
                        },
                        {
                            path: "timeline",
                            component: TimelineComponent,
                            data: {
                                title: "ระยะเวลาโครงการ",
                                keywords:
                                    "ระยะเวลาโครงการ, Store, Angular, App",
                                description:
                                    "This is timeline store angular app",
                            },
                        },
                        {
                            path: "contact",
                            component: ContactComponent,
                            data: {
                                title: "ติดต่อเรา",
                                keywords: "ติดต่อเรา, Store, Angular, App",
                                description:
                                    "This is contact store angular app",
                            },
                        },
                    ],
                },
                {
                    path: "backend",
                    component: AppMainComponent,
                    canActivate: [AuthGuard],
                    children: [{ path: "", component: DashboardDemoComponent }],
                },
                // {
                //     path: "backend/dashboard",
                //     component: AppMainComponent,
                //     canActivate:[AuthGuard],
                //     children: [{ path: "", component: DashboardDemoComponent }],
                // },
                {
                    path: "backend/admin",
                    component: AppMainComponent,
                    canActivate: [AuthGuard],
                    children: [
                        { path: "", component: MainAdminComponent },
                        { path: "manage", component: ManageAdminComponent },
                        { path: "edit", component: EditAdminComponent },
                    ],
                },
                {
                    path: "backend/role",
                    component: AppMainComponent,
                    canActivate: [AuthGuard],
                    children: [
                        { path: "", component: MainRoleComponent },
                        { path: "manage", component: AddRoleComponent },
                        { path: "edit", component: EditRoleComponent },
                    ],
                },
                {
                    path: "backend/project",
                    component: AppMainComponent,
                    canActivate: [AuthGuard],
                    children: [
                        { path: "", component: MainProjectComponent },
                        { path: "manage", component: ManageProjectComponent },
                    ],
                },
                {
                    path: "backend/participant",
                    component: AppMainComponent,
                    canActivate: [AuthGuard],
                    children: [
                        { path: "", component: MainParticipantComponent },
                        {
                            path: "manage",
                            component: ManageParticipantComponent,
                        },
                        { path: "edit", component: EditParticipantComponent },
                        { path: "view", component: ViewParticipantComponent },
                    ],
                },

                { path: "error", component: AppErrorComponent },
                { path: "access", component: AppAccessdeniedComponent },
                { path: "notfound", component: AppNotfoundComponent },
                { path: "login", component: AppLoginComponent },
                { path: "**", redirectTo: "/notfound" },
            ],
            { scrollPositionRestoration: "enabled" }
        ),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
