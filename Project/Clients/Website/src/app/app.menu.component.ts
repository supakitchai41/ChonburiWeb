import { Component, OnInit } from '@angular/core';
import { AppMainComponent } from './app.main.component';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(public app: AppMainComponent) { }

    ngOnInit() {
        this.model = [
             {label: 'Home Page', icon: 'pi pi-fw pi-home', routerLink: ['/']},
            {label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: ['/backend']},
            {label: 'จัดการ Admin', icon: 'pi pi-fw pi-home', routerLink: ['/backend/admin']},
            {label: 'จัดการ Role', icon: 'pi pi-fw pi-home', routerLink: ['/backend/role']},
            // {label: 'จัดการ Project', icon: 'pi pi-fw pi-home', routerLink: ['/backend/project']},
            {label: 'จัดการ Participant', icon: 'pi pi-fw pi-home', routerLink: ['/backend/participant']},

        ];
    }

    
}
