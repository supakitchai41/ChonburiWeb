import { Component, OnInit } from "@angular/core";

import { RoleService } from "src/app/service-api/role.service";
import { RoleResultModel, RoleModel } from "src/app/model-api/role.model";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

import {
    MessageService,
    ConfirmationService,
    ConfirmEventType,
} from "primeng/api";

@Component({
    templateUrl: "./edit-role.component.html",
    providers: [MessageService, ConfirmationService],
    styleUrls: ["edit-role.component.scss"],
})
export class EditRoleComponent implements OnInit {
    editRole: RoleModel = {} as RoleModel;
    resRoleModel: RoleResultModel;
    id: any;

    constructor(
        private roleService: RoleService,
        private cornfirmationService: ConfirmationService,
        private messageService: MessageService,
        private config: DynamicDialogConfig,
        private dialogRef: DynamicDialogRef
    ) {}

    ngOnInit() {
        this.id = this.config.data.id;

        this.roleService.getRoleById(this.id).subscribe((res) => {
            if (res.status == 200) {
                this.resRoleModel = res;
                this.editRole = this.resRoleModel.data;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });
    }

    cancelFunction() {
        this.dialogRef.close();
    }

    updateRole() {
        this.roleService
            .updateRole(this.editRole.id, this.editRole)
            .subscribe((res) => {
                if (res.status == 200) {
                    this.messageService.add({
                        severity: "success",
                        summary: "สำเร็จ",
                        detail: "แก้ไขข้อมูลสำเร็จ",
                    });
                    setTimeout(() => {
                        this.dialogRef.close("update");
                    }, 1000);
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "แก้ไขข้อมูลไม่สำเร็จ",
                    });
                }
            });
    }

    confirmUpdateRole() {
        this.cornfirmationService.confirm({
            header: "ยืนยันการแก้ไขข้อมูล",
            message: "คุณต้องการแก้ไขข้อมูลใช่หรือไม่ ?",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.updateRole();
            },
        });
    }
}
