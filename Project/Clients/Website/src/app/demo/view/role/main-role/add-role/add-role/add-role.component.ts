import { Component, OnInit } from "@angular/core";

import { RoleService } from "src/app/service-api/role.service";
import { RoleResultModel, RoleModel } from "src/app/model-api/role.model";
import { MessageService, ConfirmationService } from "primeng/api";
import { DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
    templateUrl: "./add-role.component.html",
    providers: [MessageService, ConfirmationService],
    styleUrls: ["add-role.component.scss"],
})
export class AddRoleComponent implements OnInit {
    addRole: RoleModel = {} as RoleModel;
    resRoleModel: RoleResultModel;

    constructor(
        private roleService: RoleService,
        private cornfirmationService: ConfirmationService,
        private messageService: MessageService,
        private dialogRef: DynamicDialogRef
    ) {}

    ngOnInit() {}

    cancelFunction() {
        this.dialogRef.close();
    }

    confirmCreateRole() {
        this.cornfirmationService.confirm({
            header: "เพิ่มตำแหน่ง",
            message: "คุณต้องการเพิ่มข้อมูลใช่หรือไม่",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.createRole();
            },
        });
    }

    createRole() {
        if (this.addRole.firstName.trim().length >= 2) {
            this.roleService.crateRole(this.addRole).subscribe((res) => {
                if (res.status == 200) {
                    this.messageService.add({
                        severity: "success",
                        summary: "สำเร็จ",
                        detail: "เพิ่มข้อมูลสำเร็จ",
                    });
                    setTimeout(() => {
                        this.dialogRef.close("update");
                    }, 1000);
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "เพิ่มข้อมูลไม่สำเร็จ",
                    });
                }
            });
        }else {
            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด',
                detail: 'กรุณากรอกข้อมูลให้ถูกต้อง'
            })
        }
    }
}
