import {
    RoleResultModels,
    RoleFilterModel,
    RoleModel,
    RoleResultModel,
} from "./../../../../model-api/role.model";
import { RoleService } from "./../../../../service-api/role.service";

import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";

import { Table } from "primeng/table";
import { BreadcrumbService } from "src/app/app.breadcrumb.service";
import { MessageService, ConfirmationService } from "primeng/api";
import { AddRoleComponent } from "./add-role/add-role/add-role.component";
import { EditRoleComponent } from "./edit-role/edit-role.component";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
    templateUrl: "./main-role.component.html",
    providers: [MessageService, ConfirmationService, DialogService],
    styleUrls: ["./main-role.component.scss"],
})
export class MainRoleComponent implements OnInit {
    //Dialog
    position: any;
    confirmDeleteDialog: boolean = false;

    roleModels: RoleModel[];
    addRoleModel: RoleModel = {} as RoleModel;
    editRole: RoleModel = {} as RoleModel;
    resultRoleModels: RoleResultModels;
    resultRoleModel: RoleResultModel;
    filterRoles: RoleFilterModel = {} as RoleFilterModel;

    overlayDialog: DynamicDialogRef | undefined;
    loading: boolean = true;

    @ViewChild("dt") table: Table;

    @ViewChild("filter") filter: ElementRef;

    constructor(
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private roleService: RoleService,
        private breadcrumbService: BreadcrumbService,
        private dialogService: DialogService
    ) {
        this.breadcrumbService.setItems([
            { label: "Role", routerLink: ["/backend/role"] },
        ]);
    }

    ngOnInit() {
        this.fetchRoles();
        setTimeout(() => {
            this.loading = false;
        }, 1000);
    }

    fetchRoles() {
        this.roleService.getRoles(this.filterRoles).subscribe((res) => {
            if (res.status == 200) {
                this.resultRoleModels = res;
                this.roleModels = this.resultRoleModels.data;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });
    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = "";
    }

    confirmDelete(id: number) {
        this.confirmationService.confirm({
            message: "คุณต้องการลบข้อมูลชุดนี้หรือไม่ ?",
            header: "ยืนยันการลบข้อมูล",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.roleService.deleteRole(id).subscribe((res) => {
                    if (res.status == 200) {
                        this.messageService.add({
                            severity: "success",
                            summary: "การลบข้อมูล",
                            detail: "ลบข้อมูลสำเร็จ",
                        });
                        this.roleService
                            .getRoles(this.filterRoles)
                            .subscribe((res) => {
                                if (res.status == 200) {
                                    this.resultRoleModels = res;
                                    this.roleModels = res.data;
                                } else {
                                    this.messageService.add({
                                        severity: "error",
                                        summary: "เกิดข้อผิดพลาด",
                                        detail: "ดึงข้อมูลไม่สำเร็จ",
                                    });
                                }
                            });
                    } else {
                        this.messageService.add({
                            severity: "error",
                            summary: "การลบข้อมูล",
                            detail: "ลบข้อมูลไม่สำเร็จ",
                        });
                    }
                });
            },
        });
    }

    editRoleFunction(id: number) {
        const ref = this.dialogService.open(EditRoleComponent, {
            data: {
                id: id,
            },
            header: "Edit-Role",
            width: "55%",
            contentStyle: { overflow: "auto" },
            baseZIndex: 10000,
        });
        ref.onClose.subscribe((res) => {
            if (res === "update") {
                this.fetchRoles();
            }
        });
    }

    addRoleFunction() {
        const ref = this.dialogService.open(AddRoleComponent, {
            header: "Add-Role",
            width: "55%",
            contentStyle: { overflow: "auto" },
            baseZIndex: 10000,
        });
        ref.onClose.subscribe((res) => {
            if (res === "update") {
                this.fetchRoles();
            }
        });
    }
}
