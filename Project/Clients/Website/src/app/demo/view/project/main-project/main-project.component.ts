import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Customer , Representative} from 'src/app/demo/domain/customer';
import { CustomerService } from 'src/app/demo/service/customerservice';
import { Product } from 'src/app/demo/domain/product';
import { ProductService } from 'src/app/demo/service/productservice';
import {Table} from 'primeng/table';
import { BreadcrumbService } from 'src/app/app.breadcrumb.service';
import {MessageService, ConfirmationService , ConfirmEventType} from 'primeng/api';
export interface ProjectModel {
    code?: null | string;
    description?: null | string;
    id?: number;
    name?: null | string;
  }
@Component({
    templateUrl: './main-project.component.html',
    providers: [MessageService, ConfirmationService, CustomerService ],
    styleUrls: ['./main-project.component.scss'],
    styles: [`
        :host ::ng-deep  .p-frozen-column {
            font-weight: bold;
        }

        :host ::ng-deep .p-datatable-frozen-tbody {
            font-weight: bold;
        }

        :host ::ng-deep .p-progressbar {
            height:.5rem;
        }
    `]
})
export class MainProjectComponent implements OnInit {

    //Dialog
    addProjectDialog:boolean = false;
    confirmDialog:boolean = false;
    disabledaddProjectDialog:boolean;
    ProjectModel: ProjectModel = {};
    successDialog:boolean = false;
    confirmDeleteDialog: boolean = false;
    nameHeader = '';

    value1 : any
    ProjectModels: ProjectModel[];

    activityValues: number[] = [0, 100];

    loading:boolean = true;

    @ViewChild('dt') table: Table;

    @ViewChild('filter') filter: ElementRef;
    //Dialog
    position: string;
    constructor(private customerService: CustomerService,
        private breadcrumbService: BreadcrumbService,
        private confirmationService: ConfirmationService, private messageService: MessageService) {
        this.breadcrumbService.setItems([
            {label: 'Project'},
        ]);
    }

    ngOnInit() {

        this.ProjectModels = [
            {code : 'r1' , name: 'โครงการส่งเสริมสุขภาวะที่ดีและมีสุนทรียภาพของผู้เรียน'  , description: '' , id: 1  },
            {code : 'r2' , name: 'โครงการพัฒนาระบบบริหารสถานศึกษา'  , description: '' , id: 2  }
        ];
        this.loading = false;
    }


    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = '';
    }
    confirmDelete() {
        this.confirmationService.confirm({
            message: 'คุณต้องการลบข้อมูลชุดนี้หรือไม่ ?',
            header: 'ยืนยันการลบข้อมูล',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.messageService.add({severity:'info', summary:'สำเร็จ', detail:'ลบข้อมูลสำเร็จ'});
            },
            reject: (type) => {
                switch(type) {
                    case ConfirmEventType.REJECT:
                        this.messageService.add({severity:'error', summary:'เกิดข้อผิดพลาด', detail:'ขออภัย ไม่สามารถลบข้อมูลชุดนนี้ได้'});
                    break;
                    case ConfirmEventType.CANCEL:
                        this.messageService.add({severity:'warn', summary:'ยกเลิก', detail:'ยกเลิกการลบข้อมูล'});
                    break;
                }
            }
        });
    }
    addProject() {
        this.nameHeader = 'เพิ่ม โครงการ';
        this.addProjectDialog = true;
    }
    editProject(data: ProjectModel) {
        this.nameHeader = 'แก้ไข โครงการ';
        this.ProjectModel = data;
        this.addProjectDialog = true;
    }
    viewProject(data: ProjectModel){
        this.nameHeader = 'ดู โครงการ';
        this.ProjectModel = data;

        this.addProjectDialog = true;
        this.disabledaddProjectDialog = true;
    }
    closeAddReoleDialog(){
        this.disabledaddProjectDialog = false;
        this.ProjectModel = {};
        this.addProjectDialog = false;
    }
    onconfirmDialog(){
        this.addProjectDialog = false;
        this.confirmDialog = true;
        this.onconfirmSaveDialog();
    }
    onconfirmSaveDialog(){
        this.confirmationService.confirm({
            message: 'คุณต้องการบันทึกข้อมูลชุดนี้หรือไม่ ?',
            header: 'ยืนยันการบันทึกข้อมูล',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.ProjectModels.push(this.ProjectModel);
                this.ProjectModel = {};
                this.messageService.add({severity:'info', summary:'สำเร็จ', detail:'ลบข้อมูลสำเร็จ'});
            },
            reject: (type) => {
                switch(type) {
                    case ConfirmEventType.REJECT:
                        this.messageService.add({severity:'error', summary:'เกิดข้อผิดพลาด', detail:'ขออภัย ไม่สามารถลบข้อมูลชุดนนี้ได้'});
                    break;
                    case ConfirmEventType.CANCEL:
                        this.messageService.add({severity:'warn', summary:'ยกเลิก', detail:'ยกเลิกการลบข้อมูล'});
                    break;
                }
            }
        });
    }
}