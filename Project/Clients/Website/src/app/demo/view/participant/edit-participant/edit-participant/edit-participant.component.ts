import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { HomepageService } from "src/app/service-api/homepage.service";
import { ParticiPantService } from "src/app/service-api/participant.service";
import { MessageService, ConfirmationService } from "primeng/api";

import {
    ParticipantModel,
    ParticipantResultModel,
} from "src/app/model-api/participant.model";
import { ParticipantTypeModel } from "src/app/model-api/participanttype.model";
import { SexModel } from "src/app/model-api/sex.model";
import { AreaResidenceModel } from "src/app/model-api/arearesidence.model";
import { SkillModel } from "src/app/model-api/skill.model";
import { ParticipantSkillModel } from "src/app/model-api/participantSkill.model";
import { Router } from "@angular/router";

import { BreadcrumbService } from "src/app/app.breadcrumb.service";
import { NgBlockUI, BlockUI } from "ng-block-ui";
import { ConnectableObservable } from "rxjs";

@Component({
    providers: [ConfirmationService, MessageService],
    templateUrl: "./edit-participant.component.html",
    styleUrls: ["./edit-participant.component.scss"],
})
export class EditParticipantComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;

    editParticipant: ParticipantModel = {participantSkillModels: []} as ParticipantModel;
    resParticipant: ParticipantResultModel;

    selectedSkills: ParticipantSkillModel[];
    selectedBeforeName: string;
    selectedGender: SexModel = {} as SexModel;
    selectedDate: Date;
    inputIdNo: number;

    beforeName: any;
    genderGroup: SexModel[];
    areas: AreaResidenceModel[];
    participantTypes: ParticipantTypeModel[];
    skills: SkillModel[];

    constructor(
        private homepageService: HomepageService,
        private confirmationService: ConfirmationService,
        private activatedRoute: ActivatedRoute,
        private participantService: ParticiPantService,
        private breadcrumbService: BreadcrumbService,
        private messageService: MessageService,
        private router: Router
    ) {
        this.breadcrumbService.setItems([
            { label: "Participant", routerLink: ["/backend/participant"] },
            { label: "Edit" },
        ]);
    }

    ngOnInit(): void {
        this.blockUI.start("loading");
        this.getData();
        this.setBeforeName();
        this.getParticipant();
        setTimeout(() => {
            this.blockUI.stop();
        }, 1000);
        if (!this.selectedSkills) {
            this.selectedSkills = [];
        }
    }

    getData() {
        this.homepageService.getAllHomePage().subscribe((res) => {
            if (res.status == 200) {
                this.genderGroup = res.sexModels;
                this.areas = res.areaResidenceModels;
                this.participantTypes = res.participantTypeModels;
                this.skills = res.skillModels;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });
    }

    setBeforeName() {
        this.beforeName = [
            { id: "1", name: "นาย" },
            { id: "2", name: "นางสาว" },
            { id: "3", name: "นาง" },
            { id: "4", name: "ไม่ระบุ" },
        ];
    }

    getParticipant() {
        this.activatedRoute.queryParams.subscribe((response) => {
            const id = response["id"];
            this.participantService.getParticiPantId(id).subscribe((res) => {
                if (res.status == 200) {
                    this.resParticipant = res;
                    this.editParticipant = this.resParticipant.data;
                    this.selectedSkills = this.editParticipant.participantSkillModels;
                    console.log('selectedSkill: ', this.selectedSkills);
                    this.selectedBeforeName = this.editParticipant.titleName;
                    this.inputIdNo = parseInt(this.editParticipant.idno);
                    this.selectedDate = new Date(this.editParticipant.birthday);
                    const day = this.selectedDate.getDate();
                    const months = this.selectedDate.getMonth()
                    const year = this.selectedDate.getFullYear()-543;
                    this.selectedDate = new Date(year,months,day);
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "ไม่สามารถดึงข้อมูลจากระบบได้",
                    });
                }
            });
        });
    }

    confirmEditParticipant() {
        this.confirmationService.confirm({
            header: "แก้ไขข้อมูล",
            message: "คุณต้องการแก้ไขข้อมูลใช่หรือไม่?",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.editParticipantFunction();
            }
        });
    }

    editParticipantFunction() {
        if (this.selectedDate !== null) {
            const date = String(this.selectedDate.getDate()).padStart(2, "0");
            const month = String(this.selectedDate.getMonth() + 1).padStart(2, "0");
            const years = this.selectedDate.getFullYear() + 543;
            const formattedDate = `${years}-${month}-${date}`;
            this.editParticipant.birthday = formattedDate;
        }
        if (this.selectedBeforeName !== null) {
            this.editParticipant.titleName = this.selectedBeforeName;
        }
        if(this.inputIdNo !== null) {
            this.editParticipant.idno = this.inputIdNo.toString();

        }
        this.editParticipant.participantSkillModels = this.selectedSkills;
        console.log(this.editParticipant);
        this.participantService
            .updateParticiPant(this.editParticipant.id, this.editParticipant)
            .subscribe((res) => {
                if (res.status == 200) {
                    this.messageService.add({
                        severity: "success",
                        summary: "แก้ไขข้อมูล",
                        detail: "แก้ไขข้อมูลสำเร็จ",
                    });
                    setTimeout(() => {
                        this.router.navigate(["/backend/participant"]);
                    }, 500);
                } else {
                    console.log(res);
                    console.log(this.editParticipant);
                    this.messageService.add({
                        severity: "error",
                        summary: "แก้ไขข้อมูล",
                        detail: "แก้ไขข้อมูลไม่สำเร็จ",
                    });
                }
            });
    }

    onSkillCheckboxChange(idParticipant: number, idSkill: number, checked: boolean) {
        const indexArray = this.selectedSkills.findIndex(data => data.idSkill === idSkill);
        /* ถ้าไม่มีข้อมูลให้เพิ่ม ถ้ามีให้ลบออก */
        if(checked && indexArray === -1) {
            const newArray: ParticipantSkillModel = {
                idParticipant: idParticipant,
                idSkill: idSkill
            };
            this.selectedSkills.push(newArray);
            console.log('checked');
            console.log(this.selectedSkills);
        }else if(!checked && indexArray !== -1){
            this.selectedSkills.splice(indexArray, 1);
            console.log('unchecked');
            console.log(this.selectedSkills);
        }
    }

    selectedSkill(idSkills: SkillModel): boolean {
        return this.selectedSkills.some(data => data.idSkill === idSkills.id);
    }
}
