import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";

import { Table } from "primeng/table";
import { BreadcrumbService } from "src/app/app.breadcrumb.service";
import { MessageService, ConfirmationService } from "primeng/api";

import {
    ParticipantFilterModel,
    ParticipantResultModels,
    ParticipantModel,
} from "src/app/model-api/participant.model";
import { Router } from "@angular/router";
import { ParticiPantService } from "src/app/service-api/participant.service";

@Component({
    templateUrl: "./main-participant.component.html",
    providers: [MessageService, ConfirmationService],
    styleUrls: ["./main-participant.component.scss"],
    styles: [
        `
            :host ::ng-deep .p-frozen-column {
                font-weight: bold;
            }

            :host ::ng-deep .p-datatable-frozen-tbody {
                font-weight: bold;
            }

            :host ::ng-deep .p-progressbar {
                height: 0.5rem;
            }
        `,
    ],
})
export class MainParticipantComponent implements OnInit {
    //Dialog
    addProjectDialog: boolean = false;
    confirmDialog: boolean = false;
    disabledaddProjectDialog: boolean;
    successDialog: boolean = false;
    confir;
    mDeleteDialog: boolean = false;
    nameHeader = "";

    participantsModel: ParticipantModel[]; /* get respon.data */
    addParticipant: ParticipantModel =
        {} as ParticipantModel; /* Add participant */
    filterParticipants: ParticipantFilterModel; /* http get participants */
    resParticipants: ParticipantResultModels; /* response get participants */

    loading: boolean = true;

    @ViewChild("dt") table: Table;

    @ViewChild("filter") filter: ElementRef;
    //Dialog
    position: string;
    constructor(
        private breadcrumbService: BreadcrumbService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private participantService: ParticiPantService,
        private router: Router
    ) {
        this.breadcrumbService.setItems([{ label: "Participant" }]);
    }

    ngOnInit() {
        setTimeout(() => {
            this.fetchParticipants();
            this.loading = false;
        }, 1000);
    }

    fetchParticipants() {
        this.participantService
            .getParticiPants(this.filterParticipants)
            .subscribe((res) => {
                if (res.status == 200) {
                    this.resParticipants = res;
                    this.participantsModel = this.resParticipants.data;
                    console.log(this.participantsModel);
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "ดึงข้อมูลไม่สำเร็จ",
                    });
                }
            });
    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = "";
    }

    confirmDelete(id: number) {
        this.confirmationService.confirm({
            message: "คุณต้องการลบข้อมูลชุดนี้หรือไม่ ?",
            header: "ยืนยันการลบข้อมูล",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.participantService
                    .deleteParticiPant(id)
                    .subscribe((res) => {
                        if (res.status == 200) {
                            this.messageService.add({
                                severity: "success",
                                summary: "ลบข้อมูล",
                                detail: "ลบข้อมูลสำเร็จ",
                            });
                            this.participantService
                                .getParticiPants(this.filterParticipants)
                                .subscribe((res) => {
                                    if (res.status == 200) {
                                        this.resParticipants = res;
                                        this.participantsModel =
                                            this.resParticipants.data;
                                    } else {
                                        this.messageService.add({
                                            severity: "error",
                                            summary: "เกิดข้อผิดพลาด",
                                            detail: "ดึงข้อมูลไม่สำเร็จ",
                                        });
                                    }
                                });
                        } else {
                            this.messageService.add({
                                severity: "error",
                                summary: "ลบข้อมูล",
                                detail: "ลบข้อมูลไม่สำเร็จ",
                            });
                        }
                    });
            },
        });
    }

    addProject() {
        this.nameHeader = "เพิ่ม โครงการ";
        this.addProjectDialog = true;
    }

    viewParticipant(id: number) {
        this.router.navigate(["/backend/participant/view"], {
            queryParams: { id: id },
        });
    }

    editProject(id: number) {
        this.router.navigate(["/backend/participant/edit"], {
            queryParams: { id: id },
        });
    }

}
