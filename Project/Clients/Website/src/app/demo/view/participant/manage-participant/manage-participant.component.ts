import { Component, OnInit } from "@angular/core";
import { BreadcrumbService } from "src/app/app.breadcrumb.service";
import {
    MessageService,
    ConfirmEventType,
    ConfirmationService,
} from "primeng/api";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { HomepageService } from "src/app/service-api/homepage.service";
import { ParticiPantService } from "src/app/service-api/participant.service";
import { ParticipantModel } from "src/app/model-api/participant.model";
import { ParticipantTypeModel } from "src/app/model-api/participanttype.model";
import { ParticipantSkillModel } from "src/app/model-api/participantSkill.model";
import { SexModel } from "src/app/model-api/sex.model";
import { SkillModel } from "src/app/model-api/skill.model";
import { AreaResidenceModel } from "src/app/model-api/arearesidence.model";
import { Router } from "@angular/router";

import { NgBlockUI, BlockUI } from "ng-block-ui";

@Component({
    templateUrl: "./manage-participant.component.html",
    providers: [MessageService, ConfirmationService],
})
export class ManageParticipantComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    formGroup: FormGroup;
    clicked: boolean = false;
    addParticipant: ParticipantModel = {participantSkillModels: []} as ParticipantModel;
    ariaResidenceGroup: AreaResidenceModel[];
    selectedDate: Date;
    skills: SkillModel[];
    participantTypes: ParticipantTypeModel[];
    genderGroup: SexModel[];
    beforeName: any;
    selectedSkills: any[] = [];
    constructor(
        private breadcrumbService: BreadcrumbService,
        private participantService: ParticiPantService,
        private homepageService: HomepageService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private router: Router,
        private formBuilder: FormBuilder
    ) {
        this.breadcrumbService.setItems([
            { label: "Participant", routerLink: ["/backend/participant"] },
            { label: "Manage" },
        ]);
    }

    ngOnInit() {
        this.blockUI.start("loading");
        this.fetchData();
        setTimeout(() => {
            this.blockUI.stop();
        }, 1500);

        this.formGroup = this.formBuilder.group({
            beforeName: ["1", Validators.required],
            firstName: ["", Validators.required],
            surname: ["", Validators.required],
            nickname: [""],
            gender: ["", Validators.required],
            age: ["", [Validators.required]],
            birthday: [""],
            idNumber: [""],
            area: ["", Validators.required],
            phoneNumber: [""],
            parentName: [""],
            parentPhone: [""],
            areaResidence: ["", Validators.required],
            participantType: ["", Validators.required],
            ParticipantSkillModels: this.formBuilder.array([]),
            recommentToUs: [""],
        });

        this.beforeName = [
            { id: "1", name: "นาย" },
            { id: "2", name: "นางสาว" },
            { id: "3", name: "นาง" },
            { id: "4", name: "ไม่ระบุ" },
        ];
    }

    onSkillCheckboxChange(skill: number, checked: boolean) {
        /* หาค่าของ array */
        const arrayIndex = this.addParticipant.participantSkillModels.findIndex(
            (data) => data.idSkill === skill
        );
        /* ถ้ามีให้ลบ ไม่มีให้เพิ่ม*/
        if (checked && arrayIndex === -1) {
            const newArray: ParticipantSkillModel = {
                idParticipant: 0,
                idSkill: skill,
            };
            this.addParticipant.participantSkillModels.push(newArray);
        } else if(!checked && arrayIndex !== -1) {
            this.addParticipant.participantSkillModels.splice(arrayIndex, 1);
        }
        console.log(this.addParticipant.participantSkillModels);

    }

    fetchData() {
        this.homepageService.getAllHomePage().subscribe((res) => {
            if (res.status == 200) {
                this.genderGroup = res.sexModels;
                console.log('getAllhomepage', res);
                console.log(this.genderGroup);
                this.ariaResidenceGroup = res.areaResidenceModels;
                this.participantTypes = res.participantTypeModels;
                this.skills = res.skillModels;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });
    }

    confirmAddParticipantFunction() {
        this.confirmationService.confirm({
            header: "เพิ่มผู้เข้าร่วมโครงการ",
            message: "คุณต้องการเพิ่มผู้เข้าร่วมโครงการใช่หรือไม่",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.addParticipantFunction();
            },
            reject: (type) => {
                switch (type) {
                    case ConfirmEventType.CANCEL:
                        this.messageService.add({
                            severity: "info",
                            summary: "ยกเลิกการเพิ่มข้อมูล",
                            detail: "ยกเลิกการเพิ่มข้อมูลสำเร็จ",
                        });
                        break;

                    case ConfirmEventType.REJECT:
                        this.messageService.add({
                            severity: "info",
                            summary: "ยกเลิกการเพิ่มข้อมูล",
                            detail: "ยกเลิกการเพิ่มข้อมูลสำเร็จ",
                        });
                        break;
                }
            },
        });
    }

    addParticipantFunction() {
        const formValues = this.formGroup.value;
        this.addParticipant.titleName = formValues.beforeName;
        this.addParticipant.custName = formValues.firstName;
        this.addParticipant.custSurename = formValues.surname;
        this.addParticipant.nickName = formValues.nickname;
        this.addParticipant.sex = parseInt(formValues.gender);
        this.addParticipant.idno = formValues.idNumber.toString();
        this.addParticipant.address = formValues.area;
        this.addParticipant.phone = formValues.phoneNumber;
        this.addParticipant.parentName = formValues.parentName;
        this.addParticipant.parentPhone = formValues.parentPhone;
        this.addParticipant.districtOfResidence = parseInt(
            formValues.areaResidence
        );
        this.addParticipant.participantType = parseInt(
            formValues.participantType
        );
        this.addParticipant.wouldLikeProject = formValues.recommentToUs;

        if (!isNaN(formValues.birthday)) {
            const day = String(formValues.birthday.getDate()).padStart(2, "0");
            const month = String(formValues.birthday.getMonth() + 1).padStart(
                2,
                "0"
            );
            const year = formValues.birthday.getFullYear() + 543;
            const formattedDate = `${year}-${month}-${day}`;
            this.addParticipant.birthday = formattedDate;
        } else {
            this.messageService.add({
                severity:'error',
                summary: 'เกิดข้อผิดพลาด',
                detail: 'กรุณากรอกวันเกิดให้ถูกต้อง'
            })
        }
        console.log(this.addParticipant);
        this.participantService
            .crateParticiPant(this.addParticipant)
            .subscribe((res) => {
                if (res.status == 200) {
                    this.messageService.add({
                        severity: "success",
                        summary: "สำเร็จ",
                        detail: "เพิ่มข้อมูลสำเร็จ",
                    });
                    setTimeout(() => {
                        this.router.navigate(["/backend/participant"]);
                    }, 500);
                } else {
                    console.log(res);
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "เพิ่มข้อมูลไม่สำเร็จ",
                    });
                }
            });
    }
}
