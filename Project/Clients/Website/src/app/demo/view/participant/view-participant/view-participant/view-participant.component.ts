import { Component, OnInit } from "@angular/core";
import { ParticiPantService } from "src/app/service-api/participant.service";
import { DashBoardService } from "src/app/service-api/dashboard.service";
import { BreadcrumbService } from "src/app/app.breadcrumb.service";

import {
    ParticipantModel,
    ParticipantResultModel,
} from "src/app/model-api/participant.model";
import { DashBoardResultModel } from "src/app/model-api/dashboardresultModel.model";
import { SkillModel } from "src/app/model-api/skill.model";
import { SexModel } from "src/app/model-api/sex.model";

import { ActivatedRoute } from "@angular/router";
import { AreaResidenceModel } from "src/app/model-api/arearesidence.model";
import { ParticipantTypeModel } from "src/app/model-api/participanttype.model";

import { NgBlockUI, BlockUI } from "ng-block-ui";
import { ParticipantSkillModel } from "src/app/model-api/participantSkill.model";

@Component({
    selector: "app-view-participant",
    templateUrl: "./view-participant.component.html",
})
export class ViewParticipantComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;

    selectedDate: Date;

    participant: ParticipantModel = {} as ParticipantModel;
    resParticipant: ParticipantResultModel;
    selectedSkill: ParticipantSkillModel = {} as ParticipantSkillModel;
    skills: SkillModel[];
    genderGroup: SexModel[];
    areaGroup: AreaResidenceModel[];
    participantType: ParticipantTypeModel[];
    resDashboard: DashBoardResultModel = {} as DashBoardResultModel;
    beforeName: any;

    constructor(
        private participantService: ParticiPantService,
        private breadcrumbService: BreadcrumbService,
        private activatedRoute: ActivatedRoute,
        private dashboardService: DashBoardService
    ) {
        this.breadcrumbService.setItems([
            { label: "Participant", routerLink: ["participant"] },
            { label: "View-Participant", routerLink: ["participant/view"] },
        ]);
    }

    ngOnInit(): void {
        this.getData();
        this.blockUI.start("loading");
        setTimeout(() => {
            this.blockUI.stop();
        }, 1000);
    }

    getData() {
        this.activatedRoute.queryParams.subscribe({
            next: (res) => {
                const id = res["id"];
                this.participantService
                    .getParticiPantId(id)
                    .subscribe((res) => {
                        if(res.status == 200) {
                            this.resParticipant = res;
                            this.participant = this.resParticipant.data;
                            this.selectedDate = new Date(this.participant.birthday);
                            console.log(this.participant);
                        }
                    });
            },
        });

        this.beforeName = [
            { name: "นาย" },
            { name: "นาง" },
            { name: "นางสาว" },
            { name: "ไม่ระบุ" },
        ];

        this.dashboardService.getAllDashBoard().subscribe({
            next: (res) => {
                this.resDashboard = res;
                this.skills = res.skillModels;
                this.genderGroup = res.sexModels;
                this.areaGroup = res.areaResidenceModels;
                this.participantType = res.participantTypeModels;
            },
        });
    }
}
