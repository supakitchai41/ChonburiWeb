import { CountAreaResidenceModel } from "./../../../../model-api/arearesidence.model";
import { CountSkillModel } from "./../../../../model-api/skill.model";
import { CountSexModel } from "src/app/model-api/sex.model";
import { CountParticipantTypeModel } from "src/app/model-api/participanttype.model";

import { Component, OnInit } from "@angular/core";
import { NgBlockUI, BlockUI } from "ng-block-ui";
import { Chart } from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { HomepageService } from "src/app/service-api/homepage.service";

declare var longdo: any;

@Component({
    selector: "app-overview",
    templateUrl: "./overview.component.html",
    styleUrls: ["./overview.component.scss"],
})
export class OverViewComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    constructor(private homepageService: HomepageService) {}

    public loading = false;

    /* ทักษะ */
    skillModels: CountSkillModel[];

    /* อำเภอ */
    areasModels: CountAreaResidenceModel[];

    /* เพศ */
    genderModels: CountSexModel[];

    /* ประเภทผู้เข้าร่วมโครงการ */
    participantTypeModels: CountParticipantTypeModel[];

    // ข้อมูลของ map

    ngOnInit(): void {
        this.blockUI.start("loading");
        this.fetchAPI();
        setTimeout(() => {
            this.blockUI.stop();
            this.getChart();
            this.mapAPI();
        }, 1000);
    }

    fetchAPI() {
        this.homepageService.getAllHomePage().subscribe((res) => {
            this.genderModels = res.sexModels;
            this.areasModels = res.areaResidenceModels;
            this.skillModels = res.skillModels;
            this.participantTypeModels = res.participantTypeModels;
            console.log(res);
        });
    }

    getChart() {
        this.participantTypeChart(); /* ประเภทของผู้เข้าร่วมโครงการ */
        this.genderChart(); /* เพศ */
        this.areaResidenceChart(); /* ที่อยู่อาศัย */
        this.skiilsParticipantChart(); /* ทักษะ */
    }

    /* map function */
    mapAPI() {
        const locationList = [
            { lat: `${this.areasModels[0].latitude}`, lon: `${this.areasModels[0].longitude}` },
            { lat: `${this.areasModels[1].latitude}`, lon: `${this.areasModels[1].longitude}` },
            { lat: `${this.areasModels[2].latitude}`, lon: `${this.areasModels[2].longitude}` },
            { lat: `${this.areasModels[3].latitude}`, lon: `${this.areasModels[3].longitude}` },
            { lat: `${this.areasModels[4].latitude}`, lon: `${this.areasModels[4].longitude}` },
        ];

        /* สร้างแผนที่ */
        var map = new longdo.Map({
            placeholder: document.getElementById("map"),
            zoom: 8,
            zoomRange: { min: 8, max: 16 },
        });

        map.location(
            {
                lat: 13.327504303996921,
                lon: 101.0059566668757,
            },
            true
        );

        /* Loop marker map */
        for(let i=0; i<locationList.length; i++) {
            map.Overlays.add(
                new longdo.Marker(locationList[i], {
                    title: `${this.areasModels[i].name}`,
                    icon: {
                        url: 'https://map.longdo.com/mmmap/images/pin_mark.png',
                        offset: { x: 12, y: 45 }
                      },
                    detail: `จำนวนผู้เข้าร่วมโครงการ ${this.areasModels[i].count} คน`,
                    visibleRange: { min: 8, max: 16 },
                    draggable: false,
                    weight: longdo.OverlayWeight.Top,
                })
            )
        }
        map.Ui.Toolbar.visible(false); /* ซ่อนแถบเมนู */
        map.Ui.Crosshair.visible(false); /* ซ่อนสัญลักษณ์กึ่งกลาง */
        map.Ui.Fullscreen.visible(false); /* ซ่อนการย่อขยายแผนที่ */
    }

    /* ประเภทผู้ที่เข้าร่วมโครงการ */
    participantTypeChart() {
        const myChart = new Chart("participantType", {
            type: "line",
            data: {
                labels: [
                    this.participantTypeModels[0].name,
                    this.participantTypeModels[1].name,
                    this.participantTypeModels[2].name,
                    this.participantTypeModels[3].name,
                ],
                datasets: [
                    {
                        label: "",
                        data: [
                            this.participantTypeModels[0].count,
                            this.participantTypeModels[1].count,
                            this.participantTypeModels[2].count,
                            this.participantTypeModels[3].count,
                        ],
                        backgroundColor: [
                            "rgba(255, 99, 132, 0.2)",
                            "rgba(255, 159, 64, 0.2)",
                            "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)",
                        ],
                        borderColor: [
                            "rgb(255, 99, 132)",
                            "rgb(255, 159, 64)",
                            "rgb(255, 205, 86)",
                            "rgb(75, 192, 192)",
                        ],
                        borderWidth: 1,
                    },
                ],
            },
            options: {
                plugins: {
                    legend: {
                        display: false,
                    },
                },
            },
        });
    }

    /* ตารางเพศ */
    genderChart() {
        const myChart = new Chart("genderChart", {
            type: "bar",
            data: {
                labels: [
                    this.genderModels[0].name,
                    this.genderModels[1].name,
                    this.genderModels[2].name,
                    this.genderModels[3].name,
                ],
                datasets: [
                    {
                        label: "",
                        data: [
                            this.genderModels[0].count,
                            this.genderModels[1].count,
                            this.genderModels[2].count,
                            this.genderModels[3].count,
                        ],
                        backgroundColor: [
                            "rgba(54, 162, 235, 0.2)",
                            "rgb(255, 99, 132, 0.2)",
                            "rgb(255, 205, 86, 0.2)",
                            "rgb(75, 192, 192, 0.2)",
                        ],
                        borderColor: [
                            "rgb(54, 162, 235)",
                            "rgb(255, 99, 132)",
                            "rgb(255, 205, 86)",
                            "rgb(75, 192, 192)",
                        ],
                        barPercentage: 0.7,
                        borderWidth: 1,
                    },
                ],
            },
            options: {
                plugins: {
                    legend: {
                        display: false,
                    },
                },
            },
        });
    }

    // area Chart
    areaResidenceChart() {
        const myChart = new Chart("areaResidence", {
            type: "bar",
            data: {
                labels: [
                    this.areasModels[0].name,
                    this.areasModels[1].name,
                    this.areasModels[2].name,
                    this.areasModels[3].name,
                    this.areasModels[4].name,
                ],
                datasets: [
                    {
                        label: "",
                        data: [
                            this.areasModels[0].count,
                            this.areasModels[1].count,
                            this.areasModels[2].count,
                            this.areasModels[3].count,
                            this.areasModels[4].count,
                        ],
                        backgroundColor: [
                            "rgba(255, 99, 132, 0.2)",
                            "rgba(255, 159, 64, 0.2)",
                            "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)",
                            "rgba(54, 162, 235, 0.2)",
                        ],
                        borderColor: [
                            "rgb(255, 99, 132)",
                            "rgb(255, 159, 64)",
                            "rgb(255, 205, 86)",
                            "rgb(75, 192, 192)",
                            "rgb(54, 162, 235)",
                        ],
                    },
                ],
            },
            options: {
                plugins: {
                    legend: {
                        display: false,
                    },
                },
            },
        });
    }

    skiilsParticipantChart() {
        const myChart = new Chart("skiilsParticipantChart", {
            type: "doughnut",
            data: {
                labels: [
                    this.skillModels[0].name,
                    this.skillModels[1].name,
                    this.skillModels[2].name,
                    this.skillModels[3].name,
                    this.skillModels[4].name,
                    this.skillModels[5].name,
                    this.skillModels[6].name,
                    this.skillModels[7].name,
                    this.skillModels[8].name,
                    this.skillModels[9].name,
                ],
                datasets: [
                    {
                        label: "",
                        data: [
                            this.skillModels[0].count,
                            this.skillModels[1].count,
                            this.skillModels[2].count,
                            this.skillModels[3].count,
                            this.skillModels[4].count,
                            this.skillModels[5].count,
                            this.skillModels[6].count,
                            this.skillModels[7].count,
                            this.skillModels[8].count,
                            this.skillModels[9].count,
                        ],
                        backgroundColor: [
                            "rgba(255, 99, 132, 0.6)",
                            "rgba(255, 159, 64, 0.6)",
                            "rgba(255, 205, 86, 0.6)",
                            "rgba(75, 192, 192, 0.6)",
                            "rgba(54, 162, 235, 0.6)",
                            "rgba(153, 102, 255, 0.6)",
                            "rgba(201, 203, 207, 0.6)",
                            "rgba(255, 99, 132, 0.6)",
                            "rgba(255, 159, 64, 0.6)",
                            "rgba(255, 205, 86, 0.6)",
                        ],
                        borderColor: [
                            "rgba(255, 99, 132, 0.8)",
                            "rgba(255, 159, 64, 0.8)",
                            "rgba(255, 205, 86, 0.8)",
                            "rgba(75, 192, 192, 0.8)",
                            "rgba(54, 162, 235, 0.8)",
                            "rgba(153, 102, 255, 0.8)",
                            "rgba(201, 203, 207, 0.8)",
                            "rgba(255, 99, 132, 0.8)",
                            "rgba(255, 159, 64, 0.8)",
                            "rgba(255, 205, 86, 0.8)",
                        ],
                        hoverOffset: 4,
                    },
                ],
            },
            options: {
                plugins: {
                    datalabels: {
                        color: "black",
                        font: {
                            size: 16,
                        },
                        textAlign: "center",
                    },
                    legend: {
                        display: false,
                    },
                },
            },
            plugins: [ChartDataLabels],
        });
    }
}
