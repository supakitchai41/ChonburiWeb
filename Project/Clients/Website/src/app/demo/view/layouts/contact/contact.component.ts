import { Component, OnInit } from "@angular/core";
import { NgBlockUI, BlockUI } from "ng-block-ui";
declare var longdo: any;

@Component({
    selector: "app-contact",
    templateUrl: "./contact.component.html",
    styleUrls: ["./contact.component.scss"],
})
export class ContactComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    ngOnInit(): void {
        this.blockUI.start('loading ...');
        setTimeout(()=> {
            this.blockUI.stop();
            this.mapAPI();
        }, 1000);
    }

    mapAPI() {
        var map = new longdo.Map({
            placeholder: document.getElementById("map"),
            zoom: 8,
            zoomRange: { min: 8, max: 16 },
        });

        map.location(
            {
                lat: 13.822527143150683,
                lon: 100.56208203148213,
            },
            true
        );

        var marker = new longdo.Marker(
            { lat: 13.822527143150683, lon: 100.56208203148213 },
            {
                title: "ตำแหน่งที่อยู่",
                detail: "พิกัดสถานที่ของโครงการ ",
                visibleRange: { min: 8, max: 20 },
                draggable: false /* ไม่สามารถเคลื่อนย้ายหมุดได้ */,
                weight: longdo.OverlayWeight.Top,
            }
        );
        map.Overlays.add(marker);
    }
}
