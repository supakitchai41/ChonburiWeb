import { Component, EventEmitter, Input, NgModule, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shared-pop-up-success',
  templateUrl: './pop-up-success.component.html',
  styleUrls: ['./pop-up-success.component.scss']
})


export class PopUpSuccessComponent implements OnInit {
  @Input() successDialog: boolean;
  @Input() title: string;
  @Input() description: string;
  @Output() callbackOnClick = new EventEmitter<any>();
  confirm: boolean;
  constructor(
  ) { }

  ngOnInit(): void {
  }
  // hideDialog(): void{
  //   this.successDialog = false;
  //   this.callbackOnClick.emit({confirm: false });
  // }
  // onSave(value: boolean){
  //   console.log('pop-up' , value);
  //   this.callbackOnClick.emit({confirm: value });
  // }

}
