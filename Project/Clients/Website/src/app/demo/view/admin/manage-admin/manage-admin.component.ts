import { Component, OnInit } from "@angular/core";

import { StaffService } from "src/app/service-api/staff.service";
import { RoleService } from "src/app/service-api/role.service";

import { StaffsModel } from "src/app/model-api/staffs.model";
import {
    RoleModel,
    RoleFilterModel,
    RoleResultModels,
} from "src/app/model-api/role.model";
import {
    MessageService,
    ConfirmationService,
    ConfirmEventType,
} from "primeng/api";
import { DynamicDialogRef } from "primeng/dynamicdialog";
import { NgBlockUI, BlockUI } from "ng-block-ui";

@Component({
    templateUrl: "./manage-admin.component.html",
    providers: [MessageService, ConfirmationService],
    styles: [
        `
        .error-message {
            color: red;
        }
        `
    ]
})
export class ManageAdminComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    addStaffRequest: StaffsModel = {} as StaffsModel;

    filterRoles: RoleFilterModel = {} as RoleFilterModel;

    inputNumber = "";
    roleResultModels: RoleResultModels;
    roles: RoleModel[];
    selectedRole: RoleModel = {} as RoleModel;

    constructor(
        private staffService: StaffService,
        private roleService: RoleService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private dialogRef: DynamicDialogRef
    ) {}

    ngOnInit() {
        this.fetchAPI();
        this.blockUI.start("loading");
        setTimeout(() => {
            this.blockUI.stop();
        }, 1000);
    }

    fetchAPI() {
        this.roleService.getRoles(this.filterRoles).subscribe((res) => {
            this.roleResultModels = res;
            this.roles = this.roleResultModels.data;
        });
    }

    addStaff() {
        this.addStaffRequest.role = {
            id: this.selectedRole.id,
            firstName: this.selectedRole.firstName,
        };
        this.addStaffRequest.roleId = this.selectedRole.id;
        if(this.inputNumber !== null) {
            this.addStaffRequest.phone = this.inputNumber;
        }
        if(this.addStaffRequest.email.includes('@') && this.addStaffRequest.password.trim().length >= 6) {
            this.staffService.crateStaff(this.addStaffRequest).subscribe((res) => {
                if (res.status == 200) {
                    this.messageService.add({
                        severity: "success",
                        summary: "บันทึกข้อมูล",
                        detail: "บันทึกข้อมูลสำเร็จ",
                    });
                    setTimeout(() => {
                        this.dialogRef.close("update");
                    }, 500);
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "บันทึกข้อมูลไม่สำเร็จ",
                    });
                }
            });
        }else {
            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด',
                detail: 'กรุณากรอกข้อมูลให้ถูกต้อง'
            })
        }
    }

    confirmAddStaff() {
        this.confirmationService.confirm({
            header: "บันทึกข้อมูล",
            message: "คุณต้องการบันทึกข้อมูลใช่หรือไม่",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.addStaff();
            }
        });
    }

    cancel() {
        this.dialogRef.close();
    }
}
