import { Component, OnInit } from "@angular/core";

import { StaffService } from "src/app/service-api/staff.service";
import { StaffResultModel, StaffsModel } from "src/app/model-api/staffs.model";
import { RoleService } from "src/app/service-api/role.service";
import {
    RoleFilterModel,
    RoleModel,
    RoleResultModels,
} from "src/app/model-api/role.model";

import {
    MessageService,
    ConfirmationService,
} from "primeng/api";

import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { NgBlockUI, BlockUI } from "ng-block-ui";

@Component({
    templateUrl: "./edit-admin.component.html",
    providers: [ConfirmationService, MessageService],
})
export class EditAdminComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;

    admin: StaffsModel = {} as StaffsModel;
    resultAdminModel: StaffResultModel;
    filterRoles: RoleFilterModel;
    resRoles: RoleResultModels;
    roles: RoleModel[];
    selectedRole: RoleModel = {} as RoleModel;
    inputPhoneNumber: any;
    id: any;
    constructor(
        private staffService: StaffService,
        private roleService: RoleService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private config: DynamicDialogConfig,
        private dialogRef: DynamicDialogRef
    ) {}

    ngOnInit(): void {
        this.blockUI.start();
        this.getRoles();
        this.getId();
        setTimeout(() => {
            this.blockUI.stop();
        }, 1000);
    }

    getId() {
        this.id = this.config.data.id;
        this.staffService.getStaffId(this.id).subscribe((res) => {
            if (res.status == 200) {
                this.resultAdminModel = res;
                this.admin = res.data;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });
    }

    getRoles() {
        this.roleService.getRoles(this.filterRoles).subscribe((res) => {
            if(res.status == 200) {
                this.resRoles = res;
                this.roles = this.resRoles.data;
            }else {
                this.messageService.add({
                    severity:'error',
                    summary: 'เกิดข้อผิดพลาด',
                    detail: 'ดึงข้อมูลไม่สำเร็จ'
                })
            }
            }
        );
    }

    updateStaff() {
        this.admin.role = {
            id: this.selectedRole.id,
            firstName: this.selectedRole.firstName,
        };
        this.admin.roleId = this.selectedRole.id;
        console.log(this.admin.password);
        /* รอแก้ไขการตรวจสอบ password */
        if(this.admin.email.includes('@')){
            this.staffService
            .updateStaff(this.admin.id, this.admin)
            .subscribe((response) => {
                if (response.status == 200) {
                    this.messageService.add({
                        severity: "success",
                        summary: "สำเร็จ",
                        detail: "แก้ไขข้อมูลสำเร็จ",
                    });
                    setTimeout(() => {
                        this.dialogRef.close("update");
                    }, 500);
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "อัพเดทข้อมูลไม่สำเร็จ",
                    });
                }
            });
        }else {
            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด',
                detail: 'กรุณาตรวจสอบข้อมูลให้ครบถ้วน'
            })
        }
    }

    confirmUpdateStaff() {
        this.confirmationService.confirm({
            header: "ยืนยันการแก้ไขข้อมูล",
            message: "คุณต้องการแก้ไขข้อมูลใช่หรือไม่",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.updateStaff();
            }
        });
    }

    cancel() {
        this.dialogRef.close();
    }
}
