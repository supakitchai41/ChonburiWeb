import { StaffService } from "./../../../../service-api/staff.service";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Table } from "primeng/table";
import { BreadcrumbService } from "src/app/app.breadcrumb.service";
import {
    MessageService,
    ConfirmationService,
} from "primeng/api";
import {
    StaffResultModels,
    StaffFilterModel,
    StaffsModel,
} from "src/app/model-api/staffs.model";
import { ManageAdminComponent } from "../manage-admin/manage-admin.component";
import { EditAdminComponent } from "../edit-admin/edit-admin.component";

import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { NgBlockUI, BlockUI } from "ng-block-ui";

@Component({
    templateUrl: "./main-admin.component.html",
    providers: [MessageService, ConfirmationService, DialogService],
    styleUrls: ["./main-admin.component.scss"],
    styles: [
    `
    ::ng-deep .p-dialog.p-confirm-dialog.p-component {
            width: 40vw !important;
    }

    ::ng-deep .p-dialog .p-dialog-header {
        background-color: green !important;
        color: white !important;
    }

    ::ng-deep .p-dialog .p-dialog-header .p-dialog-header-icon {
        color: white !important;
    }

    ::ng-deep .p-dialog .p-dialog-content {
        padding: 1rem;
        color: black !important;
        border-bottom: none
    }

    ::ng-deep .p-dialog .p-dialog-footer {
        color: white !important;
        padding: 1rem;
        border-top: none
    }
    `
    ]
})
export class MainAdminComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;

    staffsModel: StaffsModel[];
    staff: StaffsModel = {} as StaffsModel;

    filterStaffs: StaffFilterModel = {} as StaffFilterModel;
    resultStaffs: StaffResultModels;
    getRoles: any;
    ref: DynamicDialogRef;
    loading: boolean = true;

    @ViewChild("dt") table: Table;

    @ViewChild("filter") filter: ElementRef;

    //Dialog
    position: string;
    constructor(
        private staffService: StaffService,
        private breadcrumbService: BreadcrumbService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
        private dialogService: DialogService
    ) {
        this.breadcrumbService.setItems([{ label: "Admin", routerLink: ["/backend/admin"]}]);
    }

    ngOnInit() {
        this.fetchStaffs();
        setTimeout(() => {
            this.loading = false;
        }, 1000);
    }

    fetchStaffs() {
        this.staffService.getStaffs(this.filterStaffs).subscribe((res) => {
            if (res.status == 200) {
                this.resultStaffs = res;
                this.staffsModel = this.resultStaffs.data;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });
    }

    editStaff(id: number) {
        const ref = this.dialogService.open(EditAdminComponent, {
            data: {
                id: id
            },
            header: 'Edit admin',
            width: '70%',
            contentStyle: {
                overflow: 'auto'
            },
            baseZIndex: 10000
        })

        ref.onClose.subscribe(res => {
            if(res === 'update') {
                this.fetchStaffs();
            }
        })
    }

    addStaff() {
        const ref = this.dialogService.open(ManageAdminComponent, {
            header: 'Add admin',
            width: '60%',
            contentStyle: {
                overflow: 'auto',
            },
            baseZIndex: 10000
        })

        ref.onClose.subscribe(res => {
            if(res === 'update') {
                this.fetchStaffs();
            }
        })
    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = "";
    }
    confirmDelete(id: number) {
        this.confirmationService.confirm({
            message: "คุณต้องการลบข้อมูลชุดนี้หรือไม่ ?",
            header: "ยืนยันการลบข้อมูล",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.staffService.deleteStaff(id).subscribe((response) => {
                    if (response.status == 200) {
                        this.messageService.add({
                            severity: "success",
                            summary: "สำเร็จ",
                            detail: "ลบข้อมูลสำเร็จ",
                        });
                        this.staffService.getStaffs(this.filterStaffs).subscribe(res => {
                            this.resultStaffs = res;
                            this.staffsModel = this.resultStaffs.data;
                        })
                    } else {
                        this.messageService.add({
                            severity: "error",
                            summary: "เกิดข้อผิดพลาด",
                            detail: "ลบข้อมูลไม่สำเร็จ",
                        });
                    }
                });
            }
        });
    }
}
