import ChartDataLabels from "chartjs-plugin-datalabels";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { BreadcrumbService } from "../../app.breadcrumb.service";
import { Table } from "primeng/table";
import { ConfirmationService, MessageService } from "primeng/api";
import { NavigationEnd, Router } from "@angular/router";
import {
    ParticipantFilterModel,
    ParticipantResultModels,
    ParticipantModel,
} from "src/app/model-api/participant.model";
import { ParticiPantService } from "src/app/service-api/participant.service";
import { DashBoardService } from "src/app/service-api/dashboard.service";
import { DashBoardResultModel } from "src/app/model-api/dashboardresultModel.model";
import { NgBlockUI, BlockUI } from "ng-block-ui";

@Component({
    templateUrl: "./dashboard.component.html",
    styles: [
        `
            p-chart {
                width: 100%;
            }
        `,
    ],
})
export class DashboardDemoComponent implements OnInit {
    @BlockUI() blockUI: NgBlockUI;
    loading: boolean = true;

    filterParticipants: ParticipantFilterModel = {} as ParticipantFilterModel;
    resParticipants: ParticipantResultModels;
    participants: ParticipantModel[];
    totalParticipants: DashBoardResultModel = {} as DashBoardResultModel;

    @ViewChild("tableref") table: Table;
    @ViewChild("filter") filter: ElementRef;
    genderData: any;
    genderOptions: any;
    skillData: any;
    skillOptions: any;
    skillPlugins: any;
    participantTypeData: any;
    participantTypeOptions: any;
    areaResidenceData: any;
    areaResidenceOptions: any;
    constructor(
        private breadcrumbService: BreadcrumbService,
        private participantService: ParticiPantService,
        private dashboardService: DashBoardService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private router: Router
    ) {
        this.breadcrumbService.setItems([
            { label: "Dashboard", routerLink: [""] },
        ]);
        this.skillPlugins = [ChartDataLabels];
    }

    ngOnInit() {
        this.blockUI.start("loading");
        this.fetchData();
        setTimeout(() => {
            this.loading = false;
            this.blockUI.stop();
            this.genderChart();
            this.skiilsParticipantChart();
            this.areaResidence();
            this.participantType();
        }, 1000);
    }

    fetchData() {
        /* get total participants */
        this.dashboardService.getAllDashBoard().subscribe((res) => {
            if (res.status == 200) {
                this.totalParticipants = res;
            } else {
                this.messageService.add({
                    severity: "error",
                    summary: "เกิดข้อผิดพลาด",
                    detail: "ดึงข้อมูลไม่สำเร็จ",
                });
            }
        });

        this.participantService
            .getParticiPants(this.filterParticipants)
            .subscribe((res) => {
                if (res.status == 200) {
                    this.resParticipants = res;
                    this.participants = this.resParticipants.data;
                } else {
                    this.messageService.add({
                        severity: "error",
                        summary: "เกิดข้อผิดพลาด",
                        detail: "ดึงข้อมูลไม่สำเร็จ",
                    });
                }
            });
    }

    genderChart() {
        this.genderData = {
            labels: [
                this.totalParticipants.sexModels[0].name,
                this.totalParticipants.sexModels[1].name,
                this.totalParticipants.sexModels[2].name,
                this.totalParticipants.sexModels[3].name,
            ],
            datasets: [
                {
                    label: "",
                    backgroundColor: [
                        "rgba(54, 162, 235, 0.5)",
                        "rgb(255, 99, 132, 0.5)",
                        "rgb(255, 205, 86, 0.5)",
                        "rgb(75, 192, 192, 0.5)",
                    ],
                    borderColor: [
                        "rgb(54, 162, 235)",
                        "rgb(255, 99, 132)",
                        "rgb(255, 205, 86)",
                        "rgb(75, 192, 192)",
                    ],
                    data: [
                        this.totalParticipants.sexModels[0].count,
                        this.totalParticipants.sexModels[1].count,
                        this.totalParticipants.sexModels[2].count,
                        this.totalParticipants.sexModels[3].count,
                    ],
                },
            ],
        };

        this.genderOptions = {
            responsive: true,
            plugins: {
                legend: {
                    display: false,
                    labels: {
                        fontColor: "#A0A7B5",
                    },
                },
            },
            scales: {
                x: {
                    ticks: {
                        color: "#A0A7B5",
                    },
                    grid: {
                        color: "rgba(160, 167, 181, .3)",
                    },
                },
                y: {
                    ticks: {
                        color: "#A0A7B5",
                    },
                    grid: {
                        color: "rgba(160, 167, 181, .3)",
                    },
                },
            },
        };
    }

    skiilsParticipantChart() {
        this.skillData = {
            labels: [
                this.totalParticipants.skillModels[0].name,
                this.totalParticipants.skillModels[1].name,
                this.totalParticipants.skillModels[2].name,
                this.totalParticipants.skillModels[3].name,
                this.totalParticipants.skillModels[4].name,
                this.totalParticipants.skillModels[5].name,
                this.totalParticipants.skillModels[6].name,
                this.totalParticipants.skillModels[7].name,
                this.totalParticipants.skillModels[8].name,
                this.totalParticipants.skillModels[9].name,
            ],
            datasets: [
                {
                    label: "",
                    data: [
                        this.totalParticipants.skillModels[0].count,
                        this.totalParticipants.skillModels[1].count,
                        this.totalParticipants.skillModels[2].count,
                        this.totalParticipants.skillModels[3].count,
                        this.totalParticipants.skillModels[4].count,
                        this.totalParticipants.skillModels[5].count,
                        this.totalParticipants.skillModels[6].count,
                        this.totalParticipants.skillModels[7].count,
                        this.totalParticipants.skillModels[8].count,
                        this.totalParticipants.skillModels[9].count,
                    ],
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.6)",
                        "rgba(255, 159, 64, 0.6)",
                        "rgba(255, 205, 86, 0.6)",
                        "rgba(75, 192, 192, 0.6)",
                        "rgba(54, 162, 235, 0.6)",
                        "rgba(153, 102, 255, 0.6)",
                        "rgba(201, 203, 207, 0.6)",
                        "rgba(255, 99, 132, 0.6)",
                        "rgba(255, 159, 64, 0.6)",
                        "rgba(255, 205, 86, 0.6)",
                    ],
                    borderColor: [
                        "rgba(255, 99, 132, 0.8)",
                        "rgba(255, 159, 64, 0.8)",
                        "rgba(255, 205, 86, 0.8)",
                        "rgba(75, 192, 192, 0.8)",
                        "rgba(54, 162, 235, 0.8)",
                        "rgba(153, 102, 255, 0.8)",
                        "rgba(201, 203, 207, 0.8)",
                        "rgba(255, 99, 132, 0.8)",
                        "rgba(255, 159, 64, 0.8)",
                        "rgba(255, 205, 86, 0.8)",
                    ],
                    hoverOffset: 4,
                },
            ],
        };

        this.skillOptions = {
            responsive: true,
            tooltips: {
                position: "average",
            },
            plugins: {
                datalabels: {
                    color: "black",
                    font: {
                        size: 16,
                    },
                    textAlign: "center",
                },
                legend: {
                    display: false,
                },
            },
        };
    }

    areaResidence() {
        this.areaResidenceData = {
            labels: [
                this.totalParticipants.areaResidenceModels[0].name,
                this.totalParticipants.areaResidenceModels[1].name,
                this.totalParticipants.areaResidenceModels[2].name,
                this.totalParticipants.areaResidenceModels[3].name,
                this.totalParticipants.areaResidenceModels[4].name,
            ],
            datasets: [
                {
                    label: "",
                    data: [
                        this.totalParticipants.areaResidenceModels[0].count,
                        this.totalParticipants.areaResidenceModels[1].count,
                        this.totalParticipants.areaResidenceModels[2].count,
                        this.totalParticipants.areaResidenceModels[3].count,
                        this.totalParticipants.areaResidenceModels[4].count,
                    ],
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.6)",
                        "rgba(255, 159, 64, 0.6)",
                        "rgba(255, 205, 86, 0.6)",
                        "rgba(75, 192, 192, 0.6)",
                        "rgba(54, 162, 235, 0.6)",
                    ],
                    borderColor: [
                        "rgba(255, 99, 132, 0.8)",
                        "rgba(255, 159, 64, 0.8)",
                        "rgba(255, 205, 86, 0.8)",
                        "rgba(75, 192, 192, 0.8)",
                        "rgba(54, 162, 235, 0.8)",
                    ],
                },
            ],
        };

        this.areaResidenceOptions = {
            responsive: true,
            plugins: {
                legend: {
                    display: false,
                },
            },
        };
    }

    participantType() {
        this.participantTypeData = {
            labels: [
                this.totalParticipants.participantTypeModels[0].name,
                this.totalParticipants.participantTypeModels[1].name,
                this.totalParticipants.participantTypeModels[2].name,
                this.totalParticipants.participantTypeModels[3].name,
            ],
            datasets: [
                {
                    label: "",
                    data: [
                        this.totalParticipants.participantTypeModels[0].count,
                        this.totalParticipants.participantTypeModels[1].count,
                        this.totalParticipants.participantTypeModels[2].count,
                        this.totalParticipants.participantTypeModels[3].count,
                    ],
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.6)",
                        "rgba(255, 159, 64, 0.6)",
                        "rgba(255, 205, 86, 0.6)",
                        "rgba(75, 192, 192, 0.6)",
                    ],
                    borderColor: [
                        "rgba(255, 99, 132, 0.8)",
                        "rgba(255, 159, 64, 0.8)",
                        "rgba(255, 205, 86, 0.8)",
                        "rgba(75, 192, 192, 0.8)",
                    ],
                },
            ],
        };

        this.participantTypeOptions = {
            responsive: true,
            plugins: {
                legend: {
                    display: false,
                },
            },
        };
    }

    viewParticipant(id: number) {
        this.router.navigate(["/backend/participant/view"], {
            queryParams: { id: id },
        });
    }

    editParticipant(id: number) {
        this.router.navigate(["/backend/participant/edit"], {
            queryParams: { id: id },
        });
    }

    confirmDelete(id: number) {
        this.confirmationService.confirm({
            header: "ลบข้อมูล",
            message: "คุณต้องการลบข้อมูลใช่หรือไม่",
            icon: "pi pi-exclamation-triangle",
            accept: () => {
                this.participantService
                    .deleteParticiPant(id)
                    .subscribe((res) => {
                        if (res.status == 200) {
                            this.messageService.add({
                                severity: "success",
                                summary: "ลบข้อมูล",
                                detail: "ลบข้อมูลสำเร็จ",
                            });
                        } else {
                            this.messageService.add({
                                severity: "error",
                                summary: "ลบข้อมูล",
                                detail: "ลบข้อมูลไม่สำเร็จ",
                            });
                        }
                    });
                this.participantService
                    .getParticiPants(this.filterParticipants)
                    .subscribe((res) => {
                        if (res.status == 200) {
                            this.resParticipants = res;
                            this.participants = this.resParticipants.data;
                        } else {
                            this.messageService.add({
                                severity: "error",
                                summary: "เกิดข้อผิดพลาด",
                                detail: "ดึงข้อมูลไม่สำเร็จ",
                            });
                        }
                    });
            },
        });
    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = "";
    }
}
