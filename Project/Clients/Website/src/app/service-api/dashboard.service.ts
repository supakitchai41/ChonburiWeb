import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { DashBoardResultModel } from "../model-api/dashboardresultModel.model";
import { environment } from "src/environments/environment";
import { AuthService } from "../service/auth.service";
@Injectable({
    providedIn: "root",
})
export class DashBoardService {
    //baseApiUrl: any = environment.baseApiUrl
    token: string = "";
    constructor(private http: HttpClient, private authService: AuthService) {}
    getAllDashBoard(): Observable<DashBoardResultModel> {
        this.token = this.authService.getToken();

        let httpHandler = new HttpHeaders().set(
            "Authorization",
            "bearer " + this.token
        );
        return this.http.get<DashBoardResultModel>(
            environment.apiUrl + "/DashBoard",
            { headers: httpHandler }
        );
    }
}
