import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ResultModel } from '../model-api/result.model';
import { AllTypeInParticipantModel ,  ParticipantModel , ParticipantFilterModel , ParticipantResultModel , ParticipantResultModels } from '../model-api/participant.model';
@Injectable({
  providedIn: 'root'
})
export class ParticiPantService {
  //staff.service.baseApiUrl: any = environment.baseApiUrl

  constructor(private http: HttpClient) { }

  getParticiPants(model:ParticipantFilterModel): Observable<ParticipantResultModels>{
     return  this.http.get<ParticipantResultModels>(environment.apiUrl+'/Participant/GetParticipants');
   }
  getParticiPantId(id: number): Observable<ParticipantResultModel>{
      return  this.http.get<ParticipantResultModel>(environment.apiUrl+'/Participant/GetParticipantById/'+id);
  }
  crateParticiPant(model: ParticipantModel): Observable<ResultModel>{
    return  this.http.post<ResultModel>(environment.apiUrl+'/Participant/Create' , model);
  }
  updateParticiPant(id: number,model:ParticipantModel): Observable<ResultModel>{
    return  this.http.put<ResultModel>(environment.apiUrl+'/Participant/Update/'+id,model);
  }
  deleteParticiPant(id: number): Observable<ResultModel>{
    return  this.http.delete<ResultModel>(environment.apiUrl+'/Participant/Delete/'+id);
  }
  getAllTypeInParticipant(): Observable<AllTypeInParticipantModel>{
    return  this.http.get<AllTypeInParticipantModel>(environment.apiUrl+'/Participant/GetAllTypeInParticipant');
  }
}

//วิธีเรียก add อยู่ในคลิป 1:27.38
//วิธีเรียก edit อยู่ในคลิป 1:30.06 และ 1:49.00
// เรียก  by id 1:40.46
// เรียก ลบ 1:54.57
