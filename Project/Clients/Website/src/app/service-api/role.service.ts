import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { RoleFilterModel, RoleResultModel, RoleModel, RoleResultModels } from '../model-api/role.model';
import { ResultModel } from '../model-api/result.model';


@Injectable({
  providedIn: 'root'
})
export class RoleService {
  //baseApiUrl: any = environment.baseApiUrl

  constructor(private http: HttpClient) { }

  getRoles(roleModel: RoleFilterModel): Observable<RoleResultModels> {
    return this.http.get<RoleResultModels>(environment.apiUrl+'/Staff/GetRoles');
  }
  getRoleById(id: number): Observable<RoleResultModel> {
    return this.http.get<RoleResultModel>(environment.apiUrl+'/Staff/GetRoleById/' + id);
  }
  crateRole(addRole: RoleModel): Observable<ResultModel> {
    return this.http.post<ResultModel>(environment.apiUrl+'/Staff/CrateRole', addRole);
  }
  updateRole(id: number, roleModel: RoleModel): Observable<ResultModel> {
    return this.http.put<RoleResultModel>(environment.apiUrl+'/Staff/UpdateRol/' + id, roleModel);
  }
  deleteRole(id: number): Observable<ResultModel> {
    return this.http.delete<RoleResultModel>(environment.apiUrl+'/Staff/DeleteRole/' + id);
  }
}

//วิธีเรียก add อยู่ในคลิป 1:27.38
//วิธีเรียก edit อยู่ในคลิป 1:30.06 และ 1:49.00
// เรียก  by id 1:40.46
// เรียก ลบ 1:54.57
