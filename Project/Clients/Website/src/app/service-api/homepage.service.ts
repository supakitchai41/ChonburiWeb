import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { HomePageResultModel } from '../model-api/homepageresult.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {
  //baseApiUrl: any = environment.apiUrl

  constructor(private http: HttpClient) { }

  getAllHomePage(): Observable<HomePageResultModel>{
     console.log('HomepageService',environment.apiUrl+'/HomePage');
     let httpHandler = new HttpHeaders().set("Authorization" , "bearer "+"")
     return  this.http.get<HomePageResultModel>(environment.apiUrl+'/HomePage');
  }
}
