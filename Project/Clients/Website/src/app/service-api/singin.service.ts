import { Injectable } from '@angular/core';
// import { HttpClient , HttpHeaders } from '@angular/common/http';
 import { HttpClient  } from '@angular/common/http';
 import { environment } from 'src/environments/environment';

import { Observable } from 'rxjs';
import { SingInModel } from '../model-api/staffs.model';
import { StaffResultModel } from '../model-api/staffs.model';
import { ConstantService } from '../service/constant.service';
@Injectable({
  providedIn: 'root'
})
export class SingInService {
  constructor(private http: HttpClient , private constant : ConstantService) { }
  signIn(sign: SingInModel): Observable<StaffResultModel> {
    console.log('Sign: ',environment.apiUrl+'/SignIn');
    return this.http.post<StaffResultModel>(environment.apiUrl+'/SingIn', sign );
  }
}
