import { Injectable } from '@angular/core';
 import { HttpClient , HttpInterceptor , HttpEvent , HttpHandler, HttpRequest } from '@angular/common/http';
 import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService implements HttpInterceptor {
  constructor(private authService: AuthService) { }
  token = this.authService.getToken();
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let tokenheadfer = req.clone(
      {
        setHeaders:{
          Authorization:"bearer "+ this.token
        }
      }
    )
    return next.handle(tokenheadfer);
  }


}
