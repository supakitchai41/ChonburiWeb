import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import {
    StaffFilterModel,
    StaffResultModel,
    StaffResultModels,
    StaffsModel,
} from "../model-api/staffs.model";
import { ResultModel } from "../model-api/result.model";
@Injectable({
    providedIn: "root",
})
export class StaffService {
    // baseApiUrl: any = environment.baseApiUrl;

    constructor(private http: HttpClient) {}

    getStaffs(staffModel: StaffFilterModel): Observable<StaffResultModels> {
        return this.http.get<StaffResultModels>(
            environment.apiUrl+"/Staff/GetStaffs"
        );
    }
    getStaffId(id: number): Observable<StaffResultModel> {
        return this.http.get<StaffResultModel>(
            environment.apiUrl+"/Staff/GetStaffById/" + id
        );
    }
    crateStaff(staffModel: StaffsModel): Observable<ResultModel> {
        return this.http.post<ResultModel>(
            environment.apiUrl+"/Staff/CreateStaff",
            staffModel
        );
    }
    updateStaff(id: number, staffModel: StaffsModel): Observable<ResultModel> {
        return this.http.put<ResultModel>(
            environment.apiUrl+"/Staff/UpdateStaff/" + id,
            staffModel
        );
    }
    deleteStaff(id: number): Observable<ResultModel> {
        return this.http.delete<ResultModel>(
            environment.apiUrl+"/Staff/DeleteStaff/" + id
        );
    }
}

//วิธีเรียก add อยู่ในคลิป 1:27.38
//วิธีเรียก edit อยู่ในคลิป 1:30.06 และ 1:49.00
// เรียก  by id 1:40.46
// เรียก ลบ 1:54.57
//https://www.youtube.com/watch?v=CdE6rVfPJ9I&ab_channel=SameerSaini
